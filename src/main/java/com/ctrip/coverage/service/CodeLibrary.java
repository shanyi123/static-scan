package com.ctrip.coverage.service;

import com.ctrip.coverage.model.dto.BuildPublishDto;

public interface CodeLibrary {
    public void tfsInit(BuildPublishDto buildPublishDto);
    public void tfsGet(BuildPublishDto buildPublishDto);
    public void tfsGetCommon();
	public String tfsGetLocalPath(BuildPublishDto buildPublishDto);
	public void svnGet(BuildPublishDto buildPublishDto);
	public void gitGet(BuildPublishDto buildPublishDto);

}
