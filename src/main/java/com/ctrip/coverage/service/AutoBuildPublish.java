package com.ctrip.coverage.service;

import com.ctrip.coverage.model.entity.TfsFolderEntity;

public interface AutoBuildPublish {
     public void csharpBuildPublish(TfsFolderEntity tfsFolderEntity);
     public void javaBuildPublish();
}
