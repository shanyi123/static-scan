package com.ctrip.coverage.service;

public interface CoverageWebService {
	void coverageInstrument(String batPath, String batArg);
	void coverageMonitor(String batPath, String batArg);
	void remoteCopy(String batPath,String localPath,String remotePath);
	void collectCoverage(String batPath);
	void deleteWorkSpace(String batPath);
}
