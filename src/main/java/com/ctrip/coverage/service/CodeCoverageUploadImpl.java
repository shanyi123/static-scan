package com.ctrip.coverage.service;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.bstek.dorado.uploader.UploadProcessor;
import com.ctrip.coverage.model.dto.CoverageDto;
public class CodeCoverageUploadImpl implements UploadProcessor {
	@Autowired
	NetCodeCoverageService netCodeCoverageService;


	public Object process(MultipartFile file, HttpServletRequest req,
			HttpServletResponse res) {
		MultipartHttpServletRequest multiPartReq = (MultipartHttpServletRequest) req;
		String version = multiPartReq.getParameter("param1");
		String description = multiPartReq.getParameter("param2");
		String tfsFolderId = multiPartReq.getParameter("param3");
		try {
			description = new String(description.getBytes("ISO8859-1"), "UTF-8");  
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		
		
		try {
			InputStream fs = file.getInputStream();
			int b;
			StringBuffer sb = new StringBuffer();
			while ((b = fs.read()) != -1) {
					sb.append((char) b);
			}
			String fileContent =  new String(sb.toString().getBytes("iso8859-1"),"utf-8");
			CoverageDto coverageDto = new CoverageDto();
			coverageDto.setDescription(description);
			coverageDto.setVersion(version);
			coverageDto.setTfsFolderId(Integer.parseInt(tfsFolderId));
			coverageDto.setFile(fileContent);
			netCodeCoverageService.addNetCoverageRecord(coverageDto);
			System.out.println("dddddddddddddddddddd");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}
