package com.ctrip.coverage.service;

import com.ctrip.coverage.model.dto.BuildPublishDto;

public interface CodeBuild {
	public void csharpMsBuild(BuildPublishDto buildPublishDto);

	public String csharpDevenvBuild(BuildPublishDto buildPublishDto);
}
