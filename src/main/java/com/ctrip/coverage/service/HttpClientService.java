package com.ctrip.coverage.service;

import java.util.List;

import org.apache.http.NameValuePair;

public interface HttpClientService {
	  void requestGet(String urlWithParams) ;
	  void requestPost(String url,List<NameValuePair> params);
}
