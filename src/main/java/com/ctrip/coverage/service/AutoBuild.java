package com.ctrip.coverage.service;


public interface AutoBuild {
     public void xcopy(String localPath,String applicationPath);
     public void InstrumentAndMonitor(String applicationPath);
     public void SitRestart();
     public void collectCoverage();
     public void deleteWorkSpace();
}
