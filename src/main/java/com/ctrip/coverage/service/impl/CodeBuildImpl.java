package com.ctrip.coverage.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.ctrip.coverage.model.dto.BuildDto;
import com.ctrip.coverage.model.dto.BuildPublishDto;
import com.ctrip.coverage.service.CodeBuild;
import com.ctrip.coverage.utils.config.CodelibraryConfig;
import com.ctrip.coverage.utils.config.RCConfig;

import freemarker.template.Template;

@Component
public class CodeBuildImpl implements CodeBuild {
	@Autowired
	FreeMarkerConfigurer freeMarkerConfigurer;
	@Autowired
	RCConfig config;
	@Autowired
	CodelibraryConfig codelibraryConfig;

	public void csharpMsBuild(BuildPublishDto buildPublishDto) {
		Process proc;
		String str;
		BufferedReader bufferedReader;
		Runtime rt = Runtime.getRuntime();
		BuildDto buildDtoOut = new BuildDto();
		Boolean isBuildSuc = true;
		try {
			//编译代码
			String templateContent = "";
			Map msbuildDatMap = new HashMap();
			msbuildDatMap.put("solutionName", buildPublishDto.getProjectName() + ".sln");
			Template tpl = freeMarkerConfigurer.getConfiguration().getTemplate("msbuild.ftl");
			templateContent = FreeMarkerTemplateUtils.processTemplateIntoString(tpl, msbuildDatMap);
			File msbuildFile = new File(buildPublishDto.getLocalPath() + "/msbuild.xml");
			if (!msbuildFile.exists()) {
				msbuildFile.createNewFile();
			}
			OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(msbuildFile), "UTF-8");
			out.write(templateContent);
			out.flush();
			out.close();
			proc = rt.exec("cmd.exe /c msbuild msbuild.xml", null, new File(buildPublishDto.getLocalPath()));
			proc.getOutputStream().close();
			bufferedReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((str = bufferedReader.readLine()) != null) {
				if (str.contains("Build FAILED")) {
					isBuildSuc = false;
				}
			}
			proc.getInputStream().close();
			proc.waitFor();
			msbuildFile.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
		buildDtoOut.setIsBuildSuc(isBuildSuc);
	}

	public String csharpDevenvBuild(BuildPublishDto buildDtoIn) {
		Process proc;
		Runtime rt = Runtime.getRuntime();
		String command;
		BufferedReader bufferedReader;
		String content = "";
		command = "cmd.exe /c  start /b " + config.getBatPath() + "\\TfsDevenvBuild.bat " + buildDtoIn.getLocalPath() + "\\" + buildDtoIn.getProjectName() + ".sln";
		try {
			proc = rt.exec(command, null, new File(codelibraryConfig.getTfsPath()));
			proc.getOutputStream().close();
			StringWriter writer = new StringWriter();
			proc.getOutputStream().close();
			IOUtils.copy(proc.getInputStream(), writer, "gbk");
			proc.waitFor();
			content = writer.toString();
			proc.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return content;
	}

}
