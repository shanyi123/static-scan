package com.ctrip.coverage.service.impl;

import java.io.File;

import org.springframework.stereotype.Component;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.wc.ISVNOptions;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import com.ctrip.coverage.service.SvnCheckOutService;
@Component
public class SvnCheckOutServiceImpl implements SvnCheckOutService{

	//声明SVN客户端管理类
	private static SVNClientManager ourClientManager;

	/**
	 * @param args
	 */
	public void svnCheckOut(String svnpath, String svnDownload)  {
		//初始化支持svn://协议的库。 必须先执行此操作。		 
		
		DAVRepositoryFactory.setup();
		
		//相关变量赋值
		SVNURL repositoryURL = null;
		try {
			repositoryURL = SVNURL.parseURIEncoded(svnpath);
		} catch (SVNException e) {
			//
		}
		String name = "cn1\\gxshi";
		String password = "YLPabs96";		
		ISVNOptions options = SVNWCUtil.createDefaultOptions(true);
		
		//实例化客户端管理类
		ourClientManager = SVNClientManager.newInstance(
				(DefaultSVNOptions) options, name, password);
		
		//要把版本库的内容check out到的目录
		File wcDir = new File(svnDownload);
		
		//通过客户端管理类获得updateClient类的实例。
		SVNUpdateClient updateClient = ourClientManager.getUpdateClient();
		
		updateClient.setIgnoreExternals(false);
		
		//执行check out 操作，返回工作副本的版本号。
		long workingVersion = -1;
		try {
			workingVersion = updateClient
					.doCheckout(repositoryURL, wcDir, SVNRevision.HEAD, SVNRevision.HEAD, SVNDepth.INFINITY,false);
		} catch (SVNException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("把版本："+workingVersion+" check out 到目录："+wcDir+"中。");

	}

}
