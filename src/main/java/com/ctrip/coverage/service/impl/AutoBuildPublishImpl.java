package com.ctrip.coverage.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ctrip.coverage.model.dto.BuildPublishDto;
import com.ctrip.coverage.model.entity.TfsFolderEntity;
import com.ctrip.coverage.service.AutoBuildPublish;
import com.ctrip.coverage.service.CodeBuild;
import com.ctrip.coverage.service.CodeLibrary;
import com.ctrip.coverage.service.CoverageWebService;
import com.ctrip.coverage.utils.config.RCConfig;
import com.ctrip.coverage.utils.enums.CodeLibraryType;

@Component
public class AutoBuildPublishImpl implements AutoBuildPublish {
	@Autowired
	CodeLibrary codeLibrary;
	@Autowired
	CodeBuild codeBuild;
	@Autowired
	RCConfig config;
	@Autowired
	CoverageWebService coverageWebServiceClient;

	public void javaBuildPublish() {

	}

	public void csharpBuildPublish(TfsFolderEntity tfsFolderEntity) {
		BuildPublishDto buildPublishDto = new BuildPublishDto();
		if (tfsFolderEntity.getCodeLibraryType() == CodeLibraryType.TFS) {
			codeLibrary.tfsGetCommon();
			buildPublishDto.setServerPath(tfsFolderEntity.getServerPath());
			buildPublishDto.setTfsCollection(tfsFolderEntity.getCollection());
			codeLibrary.tfsInit(buildPublishDto);
			codeLibrary.tfsGet(buildPublishDto);
			String localPath = codeLibrary.tfsGetLocalPath(buildPublishDto);
			buildPublishDto.setLocalPath(localPath);
			buildPublishDto.setProjectName(tfsFolderEntity.getProjectName());
			codeBuild.csharpDevenvBuild(buildPublishDto);
			xcopy(localPath,tfsFolderEntity.getApplicationPath());
			coverageWebServiceClient.remoteCopy(config.getBatPath(), "\\\\192.168.81.33\\publish\\Application\\Application", config.getCoverageInterfaceTestPath() + "\\Application");
			coverageWebServiceClient.coverageInstrument(config.getBatPath(), config.getCoverageInterfaceTestPath() + "\\Application\\" + tfsFolderEntity.getApplicationPath()
			+ "\\bin");
		} else if (tfsFolderEntity.getCodeLibraryType() == CodeLibraryType.GIT) {
			codeLibrary.gitGet(buildPublishDto);
			codeBuild.csharpDevenvBuild(buildPublishDto);
//			xcopy(localPath,tfsFolderEntity.getApplicationPath());
			coverageWebServiceClient.remoteCopy(config.getBatPath(), "\\\\192.168.81.33\\publish\\Application\\Application", config.getCoverageInterfaceTestPath() + "\\Application");
			coverageWebServiceClient.coverageInstrument(config.getBatPath(), config.getCoverageInterfaceTestPath() + "\\Application\\" + tfsFolderEntity.getApplicationPath()
			+ "\\bin");
		}

	}

	public void xcopy(String localPath, String applicationPath) {
		Process proc;
		Runtime rt = Runtime.getRuntime();
		String command;
		String str;
		BufferedReader bufferedReader;
		command = "cmd.exe /c  start /b " + config.getBatPath() + "\\copy.bat " + localPath + "\\" + applicationPath + " " + config.getTransitPath() + "\\" + applicationPath + "";
		try {
			proc = rt.exec(command, null, new File("D:\\Users\\gxshi"));
			proc.getOutputStream().close();
			bufferedReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((str = bufferedReader.readLine()) != null) {
				System.out.println(str);
			}
			proc.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
