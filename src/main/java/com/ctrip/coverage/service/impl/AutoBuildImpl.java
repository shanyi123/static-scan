package com.ctrip.coverage.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.ctrip.coverage.service.AutoBuild;
import com.ctrip.coverage.utils.StreamGobbler;
import com.ctrip.coverage.utils.config.RCConfig;

@Component
public class AutoBuildImpl implements AutoBuild {
	@Autowired
	FreeMarkerConfigurer freeMarkerConfigurer;
	@Autowired
	RCConfig config;


	public void xcopy(String localPath, String applicationPath) {
		Process proc;
		Runtime rt = Runtime.getRuntime();
		String command;
		String str;
		BufferedReader bufferedReader;
		command = "cmd.exe /c  start /b " + config.getBatPath() + "\\copy.bat " + localPath + "\\" + applicationPath + " " + config.getTransitPath() + "\\" + applicationPath + "";
		try {
			proc = rt.exec(command, null, new File("D:\\Users\\gxshi"));
			proc.getOutputStream().close();
			bufferedReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((str = bufferedReader.readLine()) != null) {
				System.out.println(str);
			}
			proc.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void SitRestart() {
		Process proc;
		Runtime rt = Runtime.getRuntime();
		String command;
		BufferedReader bufferedReader;
		String str;
		command = "cmd.exe /c  start /b " + config.getBatPath() + "\\SitRestart.bat ";
		try {
			proc = rt.exec(command, null, new File("C:\\Windows\\System32\\inetsrv"));
			proc.getOutputStream().close();
			bufferedReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((str = bufferedReader.readLine()) != null) {
				System.out.println(str);
			}
			proc.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void InstrumentAndMonitor(String applicationPath) {
		Process proc;
		Runtime rt = Runtime.getRuntime();
		String command;
		BufferedReader bufferedReader;
		String str;
		command = "cmd.exe /c  start /b " + config.getBatPath() + "\\InstrumentAndMonitor.bat " + config.getCoverageInterfaceTestPath()+ "\\Application\\" + applicationPath + "\\bin "
				+ config.getCoverageInterfaceTestPath() + "\\out\\" + applicationPath + ".coverage";
		try {
			proc = rt.exec(command, null, new File("D:\\Users\\gxshi"));
			proc.getOutputStream().close();
			StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "Error");
			StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "Output");
			errorGobbler.start();
			outputGobbler.start();
			proc.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void collectCoverage() {
		Process proc;
		Runtime rt = Runtime.getRuntime();
		String command;
		BufferedReader bufferedReader;
		String str;
		command = "cmd.exe /c  start /b " + config.getBatPath() + "\\endToGetCoverage.bat ";
		try {
			proc = rt.exec(command, null, new File("D:\\Users\\gxshi"));
			proc.getOutputStream().close();
			bufferedReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((str = bufferedReader.readLine()) != null) {
				System.out.println(str);
			}
			proc.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteWorkSpace() {
		Process proc;
		Runtime rt = Runtime.getRuntime();
		String command;
		BufferedReader bufferedReader;
		String str;
		command = "cmd.exe /c  start /b " + config.getBatPath() + "\\deleteworkspace.bat ";
		try {
			proc = rt.exec(command, null, new File("D:\\Users\\gxshi"));
			proc.getOutputStream().close();
			bufferedReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((str = bufferedReader.readLine()) != null) {
				System.out.println(str);
			}
			proc.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
