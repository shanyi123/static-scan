package com.ctrip.coverage.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.wc.ISVNOptions;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import com.ctrip.coverage.model.dto.BuildPublishDto;
import com.ctrip.coverage.service.CodeLibrary;
import com.ctrip.coverage.utils.config.CodelibraryConfig;
import com.ctrip.coverage.utils.config.RCConfig;
import com.ctrip.coverage.utils.enums.TfsCollection;

@Component
public class CodeLibraryImpl implements CodeLibrary {
	@Autowired
	RCConfig config;
	@Autowired
	CodelibraryConfig codelibraryConfig;
	Process proc;
	Runtime rt = Runtime.getRuntime();
	String command;
	String str;
	BufferedReader bufferedReader;
	//声明SVN客户端管理类
	private static SVNClientManager ourClientManager;

	public void tfsInit(BuildPublishDto buildPublishDto) {
		command = "cmd.exe /c  start /b " + config.getBatPath() + "\\TfsInit.bat " + TfsCollection.getName(buildPublishDto.getTfsCollection()) + " "
				+ codelibraryConfig.getTfsMappingPath() + "\\" + TfsCollection.getName(buildPublishDto.getTfsCollection());
		try {
			proc = rt.exec(command, null, new File(codelibraryConfig.getTfsPath()));
			proc.getOutputStream().close();
			bufferedReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((str = bufferedReader.readLine()) != null) {
				System.out.println(str);
			}
			proc.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void tfsGet(BuildPublishDto buildPublishDto) {
		command = "cmd.exe /c  start /b " + config.getBatPath() + "\\TfsGet.bat " + buildPublishDto.getServerPath();
		try {
			proc = rt.exec(command, null, new File(codelibraryConfig.getTfsPath()));
			proc.getOutputStream().close();
			bufferedReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((str = bufferedReader.readLine()) != null) {
				System.out.println(str);
			}
			proc.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void tfsGetCommon() {
		command = "cmd.exe /c  start /b " + config.getBatPath() + "\\TfsGetCommon.bat " + codelibraryConfig.getTfsMappingPath() + "\\Common";
		try {
			proc = rt.exec(command, null, new File(codelibraryConfig.getTfsPath()));
			proc.getOutputStream().close();
			bufferedReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((str = bufferedReader.readLine()) != null) {
				System.out.println(str);
			}
			proc.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String tfsGetLocalPath(BuildPublishDto buildPublishDto) {
		String localPath = buildPublishDto.getServerPath().replace("$", codelibraryConfig.getTfsMappingPath() + "\\" + TfsCollection.getName(buildPublishDto.getTfsCollection()));
		localPath = localPath.replace("/", "\\");
		return localPath;
	}

	public void svnGet(BuildPublishDto buildPublishDto) {
		//初始化支持svn://协议的库。 必须先执行此操作。		 
		DAVRepositoryFactory.setup();
		//相关变量赋值
		SVNURL repositoryURL = null;
		try {
			repositoryURL = SVNURL.parseURIEncoded(buildPublishDto.getServerPath());
		} catch (SVNException e) {
		}
		ISVNOptions options = SVNWCUtil.createDefaultOptions(true);

		//实例化客户端管理类
		ourClientManager = SVNClientManager.newInstance((DefaultSVNOptions) options, codelibraryConfig.getSvnUserName(), codelibraryConfig.getSvnPassword());

		//要把版本库的内容check out到的目录
		File wcDir = new File(codelibraryConfig.getSvnMappingPath());

		//通过客户端管理类获得updateClient类的实例。
		SVNUpdateClient updateClient = ourClientManager.getUpdateClient();

		updateClient.setIgnoreExternals(false);

		//执行check out 操作，返回工作副本的版本号。
		long workingVersion = -1;
		try {
			workingVersion = updateClient.doCheckout(repositoryURL, wcDir, SVNRevision.HEAD, SVNRevision.HEAD, SVNDepth.INFINITY, false);
		} catch (SVNException e) {
			e.printStackTrace();
		}

		System.out.println("把版本：" + workingVersion + " check out 到目录：" + wcDir + "中。");

	}

	public void gitGet(BuildPublishDto codeLibraryDto) {
		command = "cmd.exe /c  start /b " + config.getBatPath() + "\\GitGet.bat " + codeLibraryDto.getServerPath();
		try {
			proc = rt.exec(command, null, new File(codelibraryConfig.getGitMappingPath()));
			proc.getOutputStream().close();
			bufferedReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			while ((str = bufferedReader.readLine()) != null) {
				System.out.println(str);
			}
			proc.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
