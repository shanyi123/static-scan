package com.ctrip.coverage.service;

import com.ctrip.coverage.model.entity.TfsFolderEntity;


public interface SonarService {
	void scanCsharpProject(TfsFolderEntity tfsFolderEntity,String version);
	void scanJavaProject(TfsFolderEntity tfsFolderEntity, String version);
}
