package com.ctrip.coverage.orm.dao.impl;

import org.springframework.stereotype.Repository;

import com.ctrip.coverage.orm.dao.BaseDaoI;

@Repository("tfsMethodDao")
public class TfsMethodDaoImpl<T> extends BaseDaoImpl<T> implements BaseDaoI<T> {
}
