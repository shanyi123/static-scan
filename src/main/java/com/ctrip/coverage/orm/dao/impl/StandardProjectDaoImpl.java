package com.ctrip.coverage.orm.dao.impl;

import org.springframework.stereotype.Repository;

import com.ctrip.coverage.orm.dao.BaseDaoI;

@Repository("standardProjectDao")
public class StandardProjectDaoImpl<T> extends BaseDaoImpl<T> implements BaseDaoI<T> {
}
