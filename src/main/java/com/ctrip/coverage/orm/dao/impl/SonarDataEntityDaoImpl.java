package com.ctrip.coverage.orm.dao.impl;

import org.springframework.stereotype.Repository;

import com.ctrip.coverage.orm.dao.BaseDaoI;

@Repository("sonarDataEntityDao")
public class SonarDataEntityDaoImpl<T> extends BaseDaoImpl<T> implements BaseDaoI<T> {
}
