package com.ctrip.coverage.orm.ibaits.dao;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;

import com.ctrip.coverage.model.dto.SonarRuleDto;
import com.ctrip.coverage.model.dto.TfsFolderDto;
import com.ctrip.coverage.model.entity.SonarDataEntity;

public interface SonarDataDaoI<T> {

    public Map<String,Float> findSonarData(Long snapId) throws DataAccessException;
    public List<SonarRuleDto> getSonarNetRule();
    public String getSonarRuleNameById(Integer ruleId);
    public List<SonarRuleDto> getSonarLanguage();
    public SonarDataEntity findSnap(TfsFolderDto tfsFolderDto);
    
}

