package com.ctrip.coverage.orm.ibaits.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.ctrip.coverage.model.entity.NetCoverageLinesEntity;
import com.ctrip.coverage.orm.ibaits.dao.NetCoverageLinesMybaitisDaoI;

@Repository("netCoverageLinesMybaitisDao")
public class NetCoverageLinesMybaitisDaoImpl extends SqlSessionDaoSupport implements
NetCoverageLinesMybaitisDaoI<NetCoverageLinesEntity> {
    @Resource
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
        super.setSqlSessionFactory(sqlSessionFactory);
    }

	public List<NetCoverageLinesEntity> findLineBySourceId(Integer startCoverageId, Integer sourceFileId) throws DataAccessException {
		Map<String,Integer> smap=new HashMap<String,Integer>();
		smap.put("startCoverageId",startCoverageId);
		smap.put("sourceFileId",sourceFileId);
		return getSqlSession().selectList("CodeCoverage.findLineBySourceId",smap);
	}

	public Integer findLineCountsByType(Integer startCoverageId,Integer sourceFileId) throws DataAccessException {
		return getSqlSession().selectOne("CodeCoverage.findLineCoverageCount",12L);
	}

	public List<NetCoverageLinesEntity> findLineByCoverageId(Integer netCoverageId) throws DataAccessException {
		return  getSqlSession().selectList("CodeCoverage.findLineByCoverageId",netCoverageId);
	}

	@Override
	public Integer findLineCountsByType(Integer snapId) throws DataAccessException {
		// TODO Auto-generated method stub
		return null;
	}


    
   



	

	
	


}
