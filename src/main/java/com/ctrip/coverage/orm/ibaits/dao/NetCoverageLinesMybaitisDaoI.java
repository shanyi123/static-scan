package com.ctrip.coverage.orm.ibaits.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.ctrip.coverage.model.entity.NetCoverageLinesEntity;

public interface NetCoverageLinesMybaitisDaoI<T> {
	public List<NetCoverageLinesEntity> findLineBySourceId(Integer startCoverageId,Integer sourceFileId) throws DataAccessException;
	public Integer findLineCountsByType(Integer snapId) throws DataAccessException;
	public  List<NetCoverageLinesEntity> findLineByCoverageId(Integer netCoverageId) throws DataAccessException;
}
