package com.ctrip.coverage.web.view.tfsscan;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bstek.dorado.annotation.DataProvider;
import com.bstek.dorado.annotation.Expose;
import com.bstek.dorado.data.entity.EntityUtils;
import com.ctrip.coverage.model.dto.BuildPublishDto;
import com.ctrip.coverage.model.dto.TfsScanDto;
import com.ctrip.coverage.model.entity.TfsDifferentFileEntity;
import com.ctrip.coverage.model.entity.TfsFolderEntity;
import com.ctrip.coverage.model.entity.TfsScanEntity;
import com.ctrip.coverage.orm.dao.BaseDaoI;
import com.ctrip.coverage.service.CodeLibrary;
import com.ctrip.coverage.service.TfsService;
import com.ctrip.coverage.utils.enums.ProjectType;

@Component
public class TfsScan {
	@Autowired
	BaseDaoI<TfsScanEntity> tfsScanDao;
	@Autowired
	BaseDaoI<TfsDifferentFileEntity> tfsDifferentFileDao;
	@Autowired
	TfsService tfsService;
	@Autowired
	BaseDaoI<TfsFolderEntity> tfsFolderDao;
	@Autowired
	CodeLibrary codeLibrary;

	@DataProvider
	public Collection<TfsFolderEntity> getTfsFolderEntity(Integer parentId) throws Exception {
		List<TfsFolderEntity> tfsFolderEntityList = tfsFolderDao.find("from TfsFolderEntity where parent.id=" + parentId);
		List<TfsFolderEntity> tfsFolderEntityListNew = new ArrayList<TfsFolderEntity>();
		for (TfsFolderEntity tfsFolderEntity : tfsFolderEntityList) {
			TfsFolderEntity tfsFolderEntityNew = EntityUtils.toEntity(tfsFolderEntity);
			if (tfsFolderEntity.getType() == ProjectType.SOLUTION) {
				EntityUtils.setValue(tfsFolderEntityNew, "treeIcon", "url(>skin>common/icons.gif) -200px -80px");
			} else {
				EntityUtils.setValue(tfsFolderEntityNew, "treeIcon", " url(>skin>common/icons.gif) -260px -40px");
			}
			if (tfsFolderEntity.getType() == ProjectType.PROJECT) {
				if (null != tfsFolderEntity.getIsChange() && tfsFolderEntity.getIsChange() == 1) {
					tfsFolderEntityListNew.add(tfsFolderEntityNew);
				}
			} else {
				tfsFolderEntityListNew.add(tfsFolderEntityNew);
			}
		}

		return tfsFolderEntityListNew;
	}

	@DataProvider
	public Collection<TfsScanEntity> getTfsScanEntity(String tfsFolderId) {
		return tfsScanDao.find("from TfsScanEntity where tfsFolder.id=" + Integer.parseInt(tfsFolderId) + "order by id desc");
	}

	@DataProvider
	public Collection<TfsDifferentFileEntity> getDiffFile(Integer tfsScanEntityId) {
		return tfsDifferentFileDao.find("from TfsDifferentFileEntity where tfsScan.id=" + tfsScanEntityId);
	}

	@Expose
	public void tfsScan(String tfsFolderId, TfsScanDto tfsScanDto) {
		try {
			TfsFolderEntity tfsFolderEntity = tfsFolderDao.get("from TfsFolderEntity where id=?", Integer.parseInt(tfsFolderId));
			tfsService.scanTfs(tfsFolderEntity, tfsScanDto);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Expose
	@Transactional
	public void deleteTfsScan(List<TfsScanEntity> tfsScanEntityList) {
		for (TfsScanEntity tfsScanEntity : tfsScanEntityList) {
			tfsScanDao.delete(tfsScanEntity);
		}
	}

	@DataProvider
	public Collection<Map<String, Object>> getPieData(TfsScanEntity tfsScanEntity) {

		if (tfsScanEntity != null) {

			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			Map<String, Object> map = null;

			double values[] = new double[] { tfsScanEntity.getAddFileCount(), tfsScanEntity.getDeleteFileCount(), tfsScanEntity.getModifyFileCount() };
			String labels[] = new String[] { "added", "delete", "modify" };

			for (int i = 0, j = values.length; i < j; i++) {
				map = new HashMap<String, Object>();
				map.put("value", values[i]);
				map.put("label", labels[i]);
				list.add(map);
			}
			return list;
		}
		return null;
	}

	@DataProvider
	public Collection<Map<String, Object>> getTfsProjectColumnData(TfsScanEntity tfsScanEntity) {

		if (tfsScanEntity != null) {

			List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
			Map<String, Object> map = null;

			double values[] = new double[] { tfsScanEntity.getTotalFileCount(), tfsScanEntity.getAllChangedFileCount() };
			String labels[] = new String[] { "totalFileCount", "allChangeFileCount" };

			for (int i = 0, j = values.length; i < j; i++) {
				map = new HashMap<String, Object>();
				map.put("value", values[i]);
				map.put("label", labels[i]);
				list.add(map);
			}
			return list;
		}
		return null;
	}

	@Expose
	public void tfsScanSolution(List<TfsFolderEntity> tfsFolderList) {
		try {
			tfsService.tfsScanSolution(tfsFolderList.get(0), tfsFolderList.get(1));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Expose
	public void getLatestVersion(TfsFolderEntity tfsFolderEntity) {
		BuildPublishDto buildPublishDto=new BuildPublishDto();
		buildPublishDto.setServerPath(tfsFolderEntity.getServerPath());
		codeLibrary.tfsGet(buildPublishDto);
	}

}
