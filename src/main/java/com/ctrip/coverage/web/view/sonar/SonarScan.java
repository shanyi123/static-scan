package com.ctrip.coverage.web.view.sonar;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bstek.dorado.annotation.DataProvider;
import com.bstek.dorado.annotation.Expose;
import com.bstek.dorado.data.entity.EntityUtils;
import com.ctrip.coverage.model.dto.SonarDataDto;
import com.ctrip.coverage.model.dto.SonarRuleDto;
import com.ctrip.coverage.model.entity.SonarDataEntity;
import com.ctrip.coverage.model.entity.TfsFolderEntity;
import com.ctrip.coverage.orm.dao.BaseDaoI;
import com.ctrip.coverage.orm.ibaits.dao.SonarDataDaoI;
import com.ctrip.coverage.service.SonarService;
import com.ctrip.coverage.utils.config.RCConfig;
import com.ctrip.coverage.utils.config.SonarConfig;
import com.ctrip.coverage.utils.enums.LanguageType;
import com.ctrip.coverage.utils.enums.ProjectType;

@Component
public class SonarScan {
	@Inject
	SonarDataDaoI<SonarDataDto> sonarDataDao;
	@Autowired
	SonarConfig sonarConfig;
	@Autowired
	BaseDaoI<TfsFolderEntity> tfsFolderDao;
	@Autowired
	BaseDaoI<SonarDataEntity> sonarDataEntityDao;
	@Autowired
	RCConfig config;
	@Autowired
	SonarService sonarService;

	@DataProvider
	public Collection<TfsFolderEntity> getTfsFolderEntity(Integer parentId) throws Exception {

		List<TfsFolderEntity> tfsFolderEntityList = tfsFolderDao.find("from TfsFolderEntity where parent.id=" + parentId);
		List<TfsFolderEntity> tfsFolderEntityListNew = new ArrayList<TfsFolderEntity>();
		for (TfsFolderEntity tfsFolderEntity : tfsFolderEntityList) {
			TfsFolderEntity tfsFolderEntityNew = EntityUtils.toEntity(tfsFolderEntity);
			EntityUtils.setValue(tfsFolderEntityNew, "version", null);
			if (tfsFolderEntity.getType() == ProjectType.SOLUTION) {
				EntityUtils.setValue(tfsFolderEntityNew, "treeIcon", "url(>skin>common/icons.gif) -200px -80px");

			} else {
				EntityUtils.setValue(tfsFolderEntityNew, "treeIcon", " url(>skin>common/icons.gif) -260px -40px");
			}
			if (tfsFolderEntity.getType() == ProjectType.PROJECT) {
				if (null != tfsFolderEntity.getIsCodeScan() && tfsFolderEntity.getIsCodeScan() == 1) {
					tfsFolderEntityListNew.add(tfsFolderEntityNew);
				}
			} else {
				tfsFolderEntityListNew.add(tfsFolderEntityNew);
			}
		}
		return tfsFolderEntityListNew;
	}

	@DataProvider
	public Collection<TfsFolderEntity> getTfsFolderEntityRoot() throws Exception {

		List<TfsFolderEntity> tfsFolderEntityList = tfsFolderDao.find("from TfsFolderEntity where type=1");
		List<TfsFolderEntity> tfsFolderEntityListNew = new ArrayList<TfsFolderEntity>();
		for (TfsFolderEntity tfsFolderEntity : tfsFolderEntityList) {
			TfsFolderEntity tfsFolderEntityNew = EntityUtils.toEntity(tfsFolderEntity);
			EntityUtils.setValue(tfsFolderEntityNew, "version", null);
			if (tfsFolderEntity.getType() == ProjectType.SOLUTION) {
				EntityUtils.setValue(tfsFolderEntityNew, "treeIcon", "url(>skin>common/icons.gif) -200px -80px");

			} else {
				EntityUtils.setValue(tfsFolderEntityNew, "treeIcon", " url(>skin>common/icons.gif) -260px -40px");
			}
			if (tfsFolderEntity.getType() == ProjectType.PROJECT) {
				if (null != tfsFolderEntity.getIsCodeScan() && tfsFolderEntity.getIsCodeScan() == 1) {
					tfsFolderEntityListNew.add(tfsFolderEntityNew);
				}
			} else {
				tfsFolderEntityListNew.add(tfsFolderEntityNew);
			}
		}
		return tfsFolderEntityListNew;
	}

	@DataProvider
	public Collection<SonarDataEntity> getSonarData(String nodeKey) throws Exception {
		List<SonarDataEntity> sonarDataEntityNewList = new ArrayList<SonarDataEntity>();
		List<TfsFolderEntity> tfsFolderEntityList = tfsFolderDao.find("FROM TfsFolderEntity WHERE type=4 and nodeKey LIKE '" + nodeKey + "%' order by id asc");
		for (TfsFolderEntity tfsFolderEntity : tfsFolderEntityList) {
			List<SonarDataEntity> sonarDataEntityList = sonarDataEntityDao.find("from SonarDataEntity where tfsFolder.id=?", tfsFolderEntity.getId());
			return sonarDataEntityList;
		}
		return sonarDataEntityNewList;
	}

	@Expose
	public void sonarExecute(TfsFolderEntity tfsFolderEntity, String version) {
		if (tfsFolderEntity.getLanguage().equals(LanguageType.CSHARP)) {
			sonarService.scanCsharpProject(tfsFolderEntity, version);
		} else if (tfsFolderEntity.getLanguage().equals(LanguageType.JAVA)) {
			sonarService.scanJavaProject(tfsFolderEntity, version);
		}

	}

	@DataProvider
	public Collection<Map<String, Object>> getSonarPieData(SonarDataEntity sonarDataDto) throws IllegalAccessException, InvocationTargetException {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		if (null != sonarDataDto && null != sonarDataDto.getInfo_violations()) {
			Map<String, Object> map = null;
			double values[] = new double[] { sonarDataDto.getBlocker_violations(), sonarDataDto.getCritical_violations(), sonarDataDto.getMajor_violations(),
					sonarDataDto.getMinor_violations(), sonarDataDto.getInfo_violations() };
			String labels[] = new String[] { "阻断问题", "严重问题", "主要问题", "次要问题", "警告信息" };

			for (int i = 0, j = values.length; i < j; i++) {
				map = new HashMap<String, Object>();
				map.put("value", values[i]);
				map.put("label", labels[i]);
				list.add(map);
			}
			return list;
		}
		return null;
	}

	@DataProvider
	public Collection<Map<String, Object>> getSonarColumnData(SonarDataEntity sonarDataDto) throws IllegalAccessException, InvocationTargetException {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		if (null != sonarDataDto && null != sonarDataDto.getInfo_violations()) {
			Map<String, Object> map = null;
			double values[] = new double[] { sonarDataDto.getNcloc(), sonarDataDto.getViolations() };
			String labels[] = new String[] { "代码行数", "违规行数" };
			for (int i = 0, j = values.length; i < j; i++) {
				map = new HashMap<String, Object>();
				map.put("value", values[i]);
				map.put("label", labels[i]);
				list.add(map);
			}
			return list;
		}
		return null;
	}

	@DataProvider
	public List<SonarRuleDto> getSonarNetRule() {
		return sonarDataDao.getSonarNetRule();
	}

	@DataProvider
	public List<SonarRuleDto> getSonarLanguage() {
		return sonarDataDao.getSonarLanguage();
	}

	@Expose
	@Transactional
	public void deleteSonarData(SonarDataEntity sonarDataEntity) {
		sonarDataEntityDao.delete(sonarDataEntity);
	}
}
