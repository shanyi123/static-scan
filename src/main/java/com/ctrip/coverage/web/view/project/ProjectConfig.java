package com.ctrip.coverage.web.view.project;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bstek.dorado.annotation.DataProvider;
import com.bstek.dorado.annotation.DataResolver;
import com.bstek.dorado.annotation.Expose;
import com.bstek.dorado.data.entity.EntityState;
import com.bstek.dorado.data.entity.EntityUtils;
import com.ctrip.coverage.model.entity.TfsFolderEntity;
import com.ctrip.coverage.orm.dao.BaseDaoI;
import com.ctrip.coverage.service.TfsService;
import com.ctrip.coverage.utils.enums.ProjectType;

@Component
public class ProjectConfig {
	@Autowired
	BaseDaoI<TfsFolderEntity> tfsFolderDao;
	@Autowired
	TfsService tfsService;

	
	@DataProvider
	public Collection<TfsFolderEntity> getTfsFolderEntity(Integer parentId) throws Exception {
		List<TfsFolderEntity> tfsFolderEntityList = tfsFolderDao.find("from TfsFolderEntity where parent.id=" + parentId);
		List<TfsFolderEntity> tfsFolderEntityListNew = new ArrayList<TfsFolderEntity>();
		for (TfsFolderEntity tfsFolderEntity : tfsFolderEntityList) {
			TfsFolderEntity tfsFolderEntityNew = EntityUtils.toEntity(tfsFolderEntity);
			if (tfsFolderEntity.getType() == ProjectType.SOLUTION) {
				EntityUtils.setValue(tfsFolderEntityNew, "treeIcon", "url(>skin>common/icons.gif) -200px -80px");
			} else {
				EntityUtils.setValue(tfsFolderEntityNew, "treeIcon", " url(>skin>common/icons.gif) -260px -40px");
			}
			tfsFolderEntityListNew.add(tfsFolderEntityNew);
		}

		return tfsFolderEntityListNew;
	}
	@DataResolver
	@Transactional
	public void saveTfsFolderEntity(Collection<TfsFolderEntity> tfsFolderCollection) {
		for (TfsFolderEntity tfsFolderEntity : tfsFolderCollection) {
			saveAll(null, tfsFolderEntity,null);
		}

	}

	public void saveAll(Integer parentId, TfsFolderEntity tfsFolderEntity,String nodeKey) {

		if (EntityState.isVisibleDirty(EntityUtils.getState(tfsFolderEntity))) {
			tfsFolderEntity.setParentId(parentId);
		}
		EntityState state = EntityUtils.getState(tfsFolderEntity);
		if (state.equals(EntityState.NEW)) {
			Serializable pKey=tfsFolderDao.saveOrReturn(tfsFolderEntity);
			tfsFolderEntity.setId(Integer.parseInt(pKey.toString()));
			tfsFolderEntity.setNodeKey(nodeKey+"."+pKey);
			tfsFolderDao.update(tfsFolderEntity);
		} else if (state.equals(EntityState.MODIFIED)) {
			tfsFolderDao.update(tfsFolderEntity);
		}
		Collection<TfsFolderEntity> tfsFolderNodes = tfsFolderEntity.getReferenceTfsFolder();		
		if (tfsFolderNodes != null) {
			for (TfsFolderEntity tfsFolderNode : tfsFolderNodes) {
				saveAll(tfsFolderEntity.getId(), tfsFolderNode,tfsFolderEntity.getNodeKey());
			}
		}
	}
	@Expose
	@Transactional
	public void deleteTfsFolderEntity(TfsFolderEntity tfsFolderEntity) {
		tfsFolderDao.executeHql("DELETE FROM TfsFolderEntity WHERE nodeKey LIKE '"+tfsFolderEntity.getNodeKey()+"%'");
	}
	
}
