package com.ctrip.coverage.utils.beans;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("Method")
public class Method
{
	int BlocksCovered;	
	int BlocksNotCovered;
	int LinesCovered;
	int LinesNotCovered;
	int LinesPartiallyCovered;
	String MethodKeyName;
	String MethodName;
	String MethodFullName;
	
	@XStreamImplicit
	List<Lines> linesList=new ArrayList<Lines>();
	
	
	
	public int getBlocksCovered()
	{
		return BlocksCovered;
	}
	public void setBlocksCovered(int BlocksCovered)
	{
		this.BlocksCovered = BlocksCovered;
	}
	public int getBlocksNotCovered()
	{
		return BlocksNotCovered;
	}
	public void setBlocksNotCovered(int BlocksNotCovered)
	{
		this.BlocksNotCovered = BlocksNotCovered;
	}
	public int getLinesCovered()
	{
		return LinesCovered;
	}
	public void setLinesCovered(int LinesCovered)
	{
		this.LinesCovered = LinesCovered;
	}
	public int getLinesNotCovered()
	{
		return LinesNotCovered;
	}
	public void setLinesNotCovered(int LinesNotCovered)
	{
		this.LinesNotCovered = LinesNotCovered;
	}
	public int getLinesPartiallyCovered()
	{
		return LinesPartiallyCovered;
	}
	public void setLinesPartiallyCovered(int LinesPartiallyCovered)
	{
		this.LinesPartiallyCovered = LinesPartiallyCovered;
	}
	public String getMethodKeyName()
	{
		return MethodKeyName;
	}
	public void setMethodKeyName(String MethodKeyName)
	{
		this.MethodKeyName = MethodKeyName;
	}
	public String getMethodName()
	{
		return MethodName;
	}
	public void setMethodName(String MethodName)
	{
		this.MethodName = MethodName;
	}
	public String getMethodFullName()
	{
		return MethodFullName;
	}
	public void setMethodFullName(String methodFullName)
	{
		MethodFullName = methodFullName;
	}
	public List<Lines> getLinesList()
	{
		return linesList;
	}
	public void setLinesList(List<Lines> linesList)
	{
		this.linesList = linesList;
	}

}
