package com.ctrip.coverage.utils.beans;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("Module")
public class Module {
	String ModuleName;
	int ImageSize;
	int ImageLinkTime;
	int LinesCovered;
	int LinesPartiallyCovered;
	int LinesNotCovered;
	int BlocksCovered;
	int BlocksNotCovered;
    @XStreamImplicit
    List<NamespaceTable> namespaceTableList=new ArrayList<NamespaceTable>();
	public String getModuleName() {
		return ModuleName;
	}
	public void setModuleName(String moduleName) {
		ModuleName = moduleName;
	}
	public int getImageSize() {
		return ImageSize;
	}
	public void setImageSize(int imageSize) {
		ImageSize = imageSize;
	}
	public int getImageLinkTime() {
		return ImageLinkTime;
	}
	public void setImageLinkTime(int imageLinkTime) {
		ImageLinkTime = imageLinkTime;
	}
	public int getLinesCovered() {
		return LinesCovered;
	}
	public void setLinesCovered(int linesCovered) {
		LinesCovered = linesCovered;
	}
	public int getLinesPartiallyCovered() {
		return LinesPartiallyCovered;
	}
	public void setLinesPartiallyCovered(int linesPartiallyCovered) {
		LinesPartiallyCovered = linesPartiallyCovered;
	}
	public int getLinesNotCovered() {
		return LinesNotCovered;
	}
	public void setLinesNotCovered(int linesNotCovered) {
		LinesNotCovered = linesNotCovered;
	}
	public int getBlocksCovered() {
		return BlocksCovered;
	}
	public void setBlocksCovered(int blocksCovered) {
		BlocksCovered = blocksCovered;
	}
	public int getBlocksNotCovered() {
		return BlocksNotCovered;
	}
	public void setBlocksNotCovered(int blocksNotCovered) {
		BlocksNotCovered = blocksNotCovered;
	}
	public List<NamespaceTable> getNamespaceTableList() {
		return namespaceTableList;
	}
	public void setNamespaceTableList(List<NamespaceTable> namespaceTableList) {
		this.namespaceTableList = namespaceTableList;
	}
}
