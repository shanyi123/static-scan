package com.ctrip.coverage.utils.beans;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("Class")
public class Class
{

	int BlocksCovered;
	int BlocksNotCovered;
	int LinesCovered;
	int LinesNotCovered;
	int LinesPartiallyCovered;
	String NamespaceKeyName;
	String ClassKeyName;
	String ClassName;

	@XStreamImplicit
	List<Method> methodList=new ArrayList<Method>();
	public int getBlocksCovered()
	{
		return BlocksCovered;
	}
	public void setBlocksCovered(int BlocksCovered)
	{
		this.BlocksCovered = BlocksCovered;
	}
	public int getBlocksNotCovered()
	{
		return BlocksNotCovered;
	}
	public void setBlocksNotCovered(int BlocksNotCovered)
	{
		this.BlocksNotCovered = BlocksNotCovered;
	}
	public int getLinesCovered()
	{
		return LinesCovered;
	}
	public void setLinesCovered(int LinesCovered)
	{
		this.LinesCovered = LinesCovered;
	}
	public int getLinesNotCovered()
	{
		return LinesNotCovered;
	}
	public void setLinesNotCovered(int LinesNotCovered)
	{
		this.LinesNotCovered = LinesNotCovered;
	}
	public int getLinesPartiallyCovered()
	{
		return LinesPartiallyCovered;
	}
	public void setLinesPartiallyCovered(int LinesPartiallyCovered)
	{
		this.LinesPartiallyCovered = LinesPartiallyCovered;
	}
	public String getNamespaceKeyName()
	{
		return NamespaceKeyName;
	}
	public void setNamespaceKeyName(String NamespaceKeyName)
	{
		this.NamespaceKeyName = NamespaceKeyName;
	}
	public String getClassKeyName()
	{
		return ClassKeyName;
	}
	public void setClassKeyName(String ClassKeyName)
	{
		this.ClassKeyName = ClassKeyName;
	}
	public String getClassName()
	{
		return ClassName;
	}
	public void setClassName(String className)
	{
		ClassName = className;
	}
	public List<Method> getMethodList()
	{
		return methodList;
	}
	public void setMethodList(List<Method> methodList)
	{
		this.methodList = methodList;
	}
	
	

}
