package com.ctrip.coverage.utils.beans;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("CoverageDSPriv")
public class NetCoverage {
    @XStreamImplicit
    List<Module> modulelist=new ArrayList<Module>();

	public List<Module> getModulelist() {
		return modulelist;
	}

	public void setModulelist(List<Module> modulelist) {
		this.modulelist = modulelist;
	}
	
	@XStreamImplicit
    List<SourceFileNames> sourceFileNamesList=new ArrayList<SourceFileNames>();

	public List<SourceFileNames> getSourceFileNamesList() {
		return sourceFileNamesList;
	}

	public void setSourceFileNamesList(List<SourceFileNames> sourceFileNamesList) {
		this.sourceFileNamesList = sourceFileNamesList;
	}
	
}
