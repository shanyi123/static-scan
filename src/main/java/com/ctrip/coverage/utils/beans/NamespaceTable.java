package com.ctrip.coverage.utils.beans;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("NamespaceTable")
public class NamespaceTable {
	int BlocksCovered;
	int BlocksNotCovered;
	int LinesCovered;
	int LinesNotCovered;
	int LinesPartiallyCovered;
	String ModuleName;
	String NamespaceKeyName;
	String NamespaceName;
	@XStreamImplicit
    List<Class> classList=new ArrayList<Class>();
	public List<Class> getClassList()
	{
		return classList;
	}
	public void setClassList(List<Class> classList)
	{
		this.classList = classList;
	}
	public int getBlocksCovered() {
		return BlocksCovered;
	}
	public void setBlocksCovered(int blocksCovered) {
		BlocksCovered = blocksCovered;
	}
	public int getBlocksNotCovered() {
		return BlocksNotCovered;
	}
	public void setBlocksNotCovered(int blocksNotCovered) {
		BlocksNotCovered = blocksNotCovered;
	}
	public int getLinesCovered() {
		return LinesCovered;
	}
	public void setLinesCovered(int linesCovered) {
		LinesCovered = linesCovered;
	}
	public int getLinesNotCovered() {
		return LinesNotCovered;
	}
	public void setLinesNotCovered(int linesNotCovered) {
		LinesNotCovered = linesNotCovered;
	}
	public int getLinesPartiallyCovered() {
		return LinesPartiallyCovered;
	}
	public void setLinesPartiallyCovered(int linesPartiallyCovered) {
		LinesPartiallyCovered = linesPartiallyCovered;
	}
	public String getModuleName() {
		return ModuleName;
	}
	public void setModuleName(String moduleName) {
		ModuleName = moduleName;
	}
	public String getNamespaceKeyName() {
		return NamespaceKeyName;
	}
	public void setNamespaceKeyName(String namespaceKeyName) {
		NamespaceKeyName = namespaceKeyName;
	}
	public String getNamespaceName() {
		return NamespaceName;
	}
	public void setNamespaceName(String namespaceName) {
		NamespaceName = namespaceName;
	}
}
