package com.ctrip.coverage.utils.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Lines")
public class Lines
{

	int LnStart;
	int ColStart;
	int LnEnd;
	int Coverage;
	int SourceFileID;
	int LineID;
	public int getLnStart()
	{
		return LnStart;
	}
	public void setLnStart(int LnStart)
	{
		this.LnStart = LnStart;
	}
	public int getColStart()
	{
		return ColStart;
	}
	public void setColStart(int ColStart)
	{
		this.ColStart = ColStart;
	}
	public int getLnEnd()
	{
		return LnEnd;
	}
	public void setLnEnd(int LnEnd)
	{
		this.LnEnd = LnEnd;
	}
	public int getCoverage()
	{
		return Coverage;
	}
	public void setCoverage(int Coverage)
	{
		this.Coverage = Coverage;
	}
	public int getSourceFileID()
	{
		return SourceFileID;
	}
	public void setSourceFileID(int SourceFileID)
	{
		this.SourceFileID = SourceFileID;
	}
	public int getLineID()
	{
		return LineID;
	}
	public void setLineID(int LineID)
	{
		this.LineID = LineID;
	}
	
	


}
