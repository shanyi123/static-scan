package com.ctrip.coverage.utils.beans;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("SourceFileNames")
public class SourceFileNames {
	int SourceFileID;
	String SourceFileName;
	public int getSourceFileID() {
		return SourceFileID;
	}
	public void setSourceFileID(int sourceFileID) {
		SourceFileID = sourceFileID;
	}
	public String getSourceFileName() {
		return SourceFileName;
	}
	public void setSourceFileName(String sourceFileName) {
		SourceFileName = sourceFileName;
	}


	
}
