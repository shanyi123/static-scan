package com.ctrip.coverage.utils.enums;

public class TfsFileChangeType {

	//文件新增
	public static final int ADD=1;
	//文件减少
	public static final int DELETE=2;
	//文件修改
	public static final int MODIFY=3;
	
}
