package com.ctrip.coverage.utils.enums;

public enum TfsCollection {
	Platform("Platform", 1), Mobil("Mobil", 2);
	// 成员变量  
	private String name;
	private int index;

	// 构造方法  
	private TfsCollection(String name, int index) {
		this.name = name;
		this.index = index;
	}

	// 普通方法  
	public static String getName(int index) {
		for (TfsCollection c : TfsCollection.values()) {
			if (c.getIndex() == index) {
				return c.name;
			}
		}
		return null;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
}
