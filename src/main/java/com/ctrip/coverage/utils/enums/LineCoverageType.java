package com.ctrip.coverage.utils.enums;

public class LineCoverageType {

	//覆盖行
	public static final int lineCov=0;
	//部分覆盖行
	public static final int lineParCov=1;
	//行未覆盖
	public static final int lineNoCov=2;
	
}
