package com.ctrip.coverage.utils.enums;

public class ProjectType {

	//文件新增
	public static final int ROOT=0;
	//文件减少
	public static final int PROJECT=1;
	//文件修改
	public static final int VERSION=2;
	
	public static final int SIT=3;
	
	public static final int SOLUTION=4;
	
}
