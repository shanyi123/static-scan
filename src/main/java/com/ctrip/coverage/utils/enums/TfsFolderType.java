package com.ctrip.coverage.utils.enums;

public class TfsFolderType {

	//文件新增
	public static final int ROOT=0;
	//文件减少
	public static final int PROJECT=1;
	//文件修改
	public static final int VERSION=2;
	
	public static final int SOLUTION=3;
	
}
