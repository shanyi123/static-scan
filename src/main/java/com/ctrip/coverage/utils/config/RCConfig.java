package com.ctrip.coverage.utils.config;

public class RCConfig {
	private String tfsUserName;
	private String tfsPassword;
	private String sonarUrl;
	private String batPath;
	private String transitPath;
	private String coverageInterfaceTestPath;

	public String getTfsUserName() {
		return tfsUserName;
	}

	public void setTfsUserName(String tfsUserName) {
		this.tfsUserName = tfsUserName;
	}

	public String getTfsPassword() {
		return tfsPassword;
	}

	public void setTfsPassword(String tfsPassword) {
		this.tfsPassword = tfsPassword;
	}

	public String getSonarUrl() {
		return sonarUrl;
	}

	public void setSonarUrl(String sonarUrl) {
		this.sonarUrl = sonarUrl;
	}

	public String getBatPath() {
		return batPath;
	}

	public void setBatPath(String batPath) {
		this.batPath = batPath;
	}

	public String getTransitPath() {
		return transitPath;
	}

	public void setTransitPath(String transitPath) {
		this.transitPath = transitPath;
	}

	public String getCoverageInterfaceTestPath() {
		return coverageInterfaceTestPath;
	}

	public void setCoverageInterfaceTestPath(String coverageInterfaceTestPath) {
		this.coverageInterfaceTestPath = coverageInterfaceTestPath;
	}

}
