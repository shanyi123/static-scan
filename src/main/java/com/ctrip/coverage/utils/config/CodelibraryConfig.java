package com.ctrip.coverage.utils.config;

public class CodelibraryConfig {
	private String tfsVcPath;
	private String tfsMappingPath;
	private String tfsPath;
	private String svnMappingPath;
	private String svnUserName;
	private String svnPassword;
	private String gitMappingPath;

	public String getTfsVcPath() {
		return tfsVcPath;
	}

	public void setTfsVcPath(String tfsVcPath) {
		this.tfsVcPath = tfsVcPath;
	}

	public String getTfsMappingPath() {
		return tfsMappingPath;
	}

	public void setTfsMappingPath(String tfsMappingPath) {
		this.tfsMappingPath = tfsMappingPath;
	}

	public String getTfsPath() {
		return tfsPath;
	}

	public void setTfsPath(String tfsPath) {
		this.tfsPath = tfsPath;
	}

	public String getSvnMappingPath() {
		return svnMappingPath;
	}

	public void setSvnMappingPath(String svnMappingPath) {
		this.svnMappingPath = svnMappingPath;
	}

	public String getSvnUserName() {
		return svnUserName;
	}

	public void setSvnUserName(String svnUserName) {
		this.svnUserName = svnUserName;
	}

	public String getSvnPassword() {
		return svnPassword;
	}

	public void setSvnPassword(String svnPassword) {
		this.svnPassword = svnPassword;
	}

	public String getGitMappingPath() {
		return gitMappingPath;
	}

	public void setGitMappingPath(String gitMappingPath) {
		this.gitMappingPath = gitMappingPath;
	}

}
