package com.ctrip.coverage.utils.config;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import org.springframework.stereotype.Component;

/**
 *对属性文件（xx.properties）的操作
 *注：属性文件一定要放在当前工程的根目录下，也就是放在与src目录在同一个目录下（我的JDevelop
 *是这样的）
 */
@Component
public class SonarConfig {
	public SonarConfig() {
	}

	/**
	 *采用Properties类取得属性文件对应值
	 *@parampropertiesFileNameproperties文件名，如a.properties
	 *@parampropertyName属性名
	 *@return根据属性名得到的属性值，如没有返回""
	 */
	public String getValueByPropertyName(String propertiesFileName, String propertyName) {
		String s = "";
		Properties p = new Properties();//加载属性文件读取类
		FileInputStream in;
		try {
			//propertiesFileName如test.properties
			in = new FileInputStream(propertiesFileName);//以流的形式读入属性文件
			p.load(in);//属性文件将该流加入的可被读取的属性中
			in.close();//读完了关闭
			s = p.getProperty(propertyName);//取得对应的属性值
		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}

	/**
	 *采用ResourceBundel类取得属性文件对应值，这个只能够读取，不可以更改及写新的属性
	 *@parampropertiesFileNameWithoutPostfixproperties文件名，不带后缀
	 *@parampropertyName属性名
	 *@return根据属性名得到的属性值，如没有返回""
	 */
	public String getValueByPropertyName_(String propertiesFileNameWithoutPostfix, String propertyName) {
		String s = "";
		//如属性文件是test.properties，那此时propertiesFileNameWithoutPostfix的值就是test
		ResourceBundle bundel = ResourceBundle.getBundle(propertiesFileNameWithoutPostfix);
		s = bundel.getString(propertyName);
		return s;
	}

	/**
	 *更改属性文件的值，如果对应的属性不存在，则自动增加该属性
	 *@parampropertiesFileNameproperties文件名，如a.properties
	 *@parampropertyName属性名
	 *@parampropertyValue将属性名更改成该属性值
	 *@return是否操作成功
	 */
	public boolean changeValueByPropertyName(String propertiesFileName, Map propertyMap) {
		boolean writeOK = true;
		Properties p = new Properties();
		FileInputStream in;
		try {
			in = new FileInputStream(propertiesFileName);
			p.load(in);
			in.close();
			Iterator it = propertyMap.entrySet().iterator();
			FileOutputStream out = new FileOutputStream(propertiesFileName);
			while (it.hasNext()) {
				Map.Entry entry = (Map.Entry) it.next();
				Object key = entry.getKey();
				Object value = entry.getValue();
				p.setProperty(key.toString(), value.toString());
			}
			p.store(out, "");
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return writeOK;
	}

}