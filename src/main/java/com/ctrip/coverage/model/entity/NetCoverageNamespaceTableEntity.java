package com.ctrip.coverage.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "peter_nc_namespace")
public class NetCoverageNamespaceTableEntity {
	Integer id;
	int blocksCovered;
	int blocksNotCovered;
	int linesCovered;
	int linesNotCovered;
	int linesPartiallyCovered;
	String moduleName;
	String namespaceKeyName;
	String namespaceName;
	NetCoverageModuleEntity netCoverageModule;
	List<NetCoverageClassEntity> classList=new ArrayList<NetCoverageClassEntity>();
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public int getBlocksCovered() {
		return blocksCovered;
	}
	public void setBlocksCovered(int blocksCovered) {
		this.blocksCovered = blocksCovered;
	}
	public int getBlocksNotCovered() {
		return blocksNotCovered;
	}
	public void setBlocksNotCovered(int blocksNotCovered) {
		this.blocksNotCovered = blocksNotCovered;
	}
	public int getLinesCovered() {
		return linesCovered;
	}
	public void setLinesCovered(int linesCovered) {
		this.linesCovered = linesCovered;
	}
	public int getLinesNotCovered() {
		return linesNotCovered;
	}
	public void setLinesNotCovered(int linesNotCovered) {
		this.linesNotCovered = linesNotCovered;
	}
	public int getLinesPartiallyCovered() {
		return linesPartiallyCovered;
	}
	public void setLinesPartiallyCovered(int linesPartiallyCovered) {
		this.linesPartiallyCovered = linesPartiallyCovered;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getNamespaceKeyName() {
		return namespaceKeyName;
	}
	public void setNamespaceKeyName(String namespaceKeyName) {
		this.namespaceKeyName = namespaceKeyName;
	}
	public String getNamespaceName() {
		return namespaceName;
	}
	public void setNamespaceName(String namespaceName) {
		this.namespaceName = namespaceName;
	}
	@OneToMany(cascade={CascadeType.ALL},mappedBy="netCoverageNamespaceTable")
	public List<NetCoverageClassEntity> getClassList()
	{
		return classList;
	}
	public void setClassList(List<NetCoverageClassEntity> classList)
	{
		this.classList = classList;
	}
	@ManyToOne
	@JoinColumn(name="nc_module_id")
	public NetCoverageModuleEntity getNetCoverageModule() {
		return netCoverageModule;
	}
	public void setNetCoverageModule(NetCoverageModuleEntity netCoverageModule) {
		this.netCoverageModule = netCoverageModule;
	}
}
