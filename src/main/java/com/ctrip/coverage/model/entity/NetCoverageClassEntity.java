package com.ctrip.coverage.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "peter_nc_class")
public class NetCoverageClassEntity
{	
	Integer id;
	int blocksCovered;
	int blocksNotCovered;
	int linesCovered;
	int linesNotCovered;
	int linesPartiallyCovered;
	String namespaceKeyName;
	String classKeyName;
	String className;
	List<NetCoverageMethodEntity> methodList=new ArrayList<NetCoverageMethodEntity>();
	NetCoverageNamespaceTableEntity netCoverageNamespaceTable;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public int getBlocksCovered() {
		return blocksCovered;
	}
	public void setBlocksCovered(int blocksCovered) {
		this.blocksCovered = blocksCovered;
	}
	public int getBlocksNotCovered() {
		return blocksNotCovered;
	}
	public void setBlocksNotCovered(int blocksNotCovered) {
		this.blocksNotCovered = blocksNotCovered;
	}
	public int getLinesCovered() {
		return linesCovered;
	}
	public void setLinesCovered(int linesCovered) {
		this.linesCovered = linesCovered;
	}
	public int getLinesNotCovered() {
		return linesNotCovered;
	}
	public void setLinesNotCovered(int linesNotCovered) {
		this.linesNotCovered = linesNotCovered;
	}
	public int getLinesPartiallyCovered() {
		return linesPartiallyCovered;
	}
	public void setLinesPartiallyCovered(int linesPartiallyCovered) {
		this.linesPartiallyCovered = linesPartiallyCovered;
	}
	
	public String getNamespaceKeyName() {
		return namespaceKeyName;
	}
	public void setNamespaceKeyName(String namespaceKeyName) {
		this.namespaceKeyName = namespaceKeyName;
	}
	
	public String getClassKeyName()
	{
		return classKeyName;
	}
	public void setClassKeyName(String classKeyName)
	{
		this.classKeyName = classKeyName;
	}
	public String getClassName()
	{
		return className;
	}
	public void setClassName(String className)
	{
		this.className = className;
	}
	@OneToMany(cascade={CascadeType.ALL},mappedBy="netCoverageClass")
	public List<NetCoverageMethodEntity> getMethodList()
	{
		return methodList;
	}
	public void setMethodList(List<NetCoverageMethodEntity> methodList)
	{
		this.methodList = methodList;
	}
	@ManyToOne
	@JoinColumn(name="nc_namespceTable_id")
	public NetCoverageNamespaceTableEntity getNetCoverageNamespaceTable()
	{
		return netCoverageNamespaceTable;
	}
	public void setNetCoverageNamespaceTable(
			NetCoverageNamespaceTableEntity netCoverageNamespaceTable)
	{
		this.netCoverageNamespaceTable = netCoverageNamespaceTable;
	}
	
	

}
