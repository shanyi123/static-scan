package com.ctrip.coverage.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "peter_nc_lines")
public class NetCoverageLinesEntity
{
	Integer id;	
	int lnStart;
	int colStart;
	int lnEnd;
	int coverage;
	int sourceFileID;
	int lineID;
	NetCoverageMethodEntity netCoverageMethod;

	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public int getLnStart()
	{
		return lnStart;
	}
	public void setLnStart(int lnStart)
	{
		this.lnStart = lnStart;
	}
	public int getColStart()
	{
		return colStart;
	}
	public void setColStart(int colStart)
	{
		this.colStart = colStart;
	}
	public int getLnEnd()
	{
		return lnEnd;
	}
	public void setLnEnd(int lnEnd)
	{
		this.lnEnd = lnEnd;
	}
	public int getCoverage()
	{
		return coverage;
	}
	public void setCoverage(int coverage)
	{
		this.coverage = coverage;
	}
	public int getSourceFileID()
	{
		return sourceFileID;
	}
	public void setSourceFileID(int sourceFileID)
	{
		this.sourceFileID = sourceFileID;
	}
	public int getLineID()
	{
		return lineID;
	}
	public void setLineID(int lineID)
	{
		this.lineID = lineID;
	}
	
	@ManyToOne
	@JoinColumn(name="nc_method_id")
	public NetCoverageMethodEntity getNetCoverageMethod()
	{
		return netCoverageMethod;
	}
	public void setNetCoverageMethod(NetCoverageMethodEntity netCoverageMethod)
	{
		this.netCoverageMethod = netCoverageMethod;
	}


	

}
