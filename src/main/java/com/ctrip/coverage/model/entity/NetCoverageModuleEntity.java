package com.ctrip.coverage.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name = "peter_nc_module")
public class NetCoverageModuleEntity {
	Integer id;
	String ModuleName;
	int ImageSize;
	int ImageLinkTime;
	int LinesCovered;
	int linesPartiallyCovered;
	int linesNotCovered;
	int blocksCovered;
	int blocksNotCovered;
	NetCoverageEntity netCoverage;
    List<NetCoverageNamespaceTableEntity> namespaceTableList=new ArrayList<NetCoverageNamespaceTableEntity>();
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getModuleName() {
		return ModuleName;
	}
	public void setModuleName(String moduleName) {
		ModuleName = moduleName;
	}
	public int getImageSize() {
		return ImageSize;
	}
	public void setImageSize(int imageSize) {
		ImageSize = imageSize;
	}
	public int getImageLinkTime() {
		return ImageLinkTime;
	}
	public void setImageLinkTime(int imageLinkTime) {
		ImageLinkTime = imageLinkTime;
	}
	public int getLinesCovered() {
		return LinesCovered;
	}
	public void setLinesCovered(int linesCovered) {
		LinesCovered = linesCovered;
	}
	public int getLinesPartiallyCovered() {
		return linesPartiallyCovered;
	}
	public void setLinesPartiallyCovered(int linesPartiallyCovered) {
		this.linesPartiallyCovered = linesPartiallyCovered;
	}
	public int getLinesNotCovered() {
		return linesNotCovered;
	}
	public void setLinesNotCovered(int linesNotCovered) {
		this.linesNotCovered = linesNotCovered;
	}
	public int getBlocksCovered() {
		return blocksCovered;
	}
	public void setBlocksCovered(int blocksCovered) {
		this.blocksCovered = blocksCovered;
	}
	public int getBlocksNotCovered() {
		return blocksNotCovered;
	}
	public void setBlocksNotCovered(int blocksNotCovered) {
		this.blocksNotCovered = blocksNotCovered;
	}
	@OneToMany(cascade={CascadeType.ALL},mappedBy="netCoverageModule")
	public List<NetCoverageNamespaceTableEntity> getNamespaceTableList() {
		return namespaceTableList;
	}
	public void setNamespaceTableList(List<NetCoverageNamespaceTableEntity> namespaceTableList) {
		this.namespaceTableList = namespaceTableList;
	}
	
	@ManyToOne
	@JoinColumn(name="nc_id")
	public NetCoverageEntity getNetCoverage() {
		return netCoverage;
	}
	public void setNetCoverage(NetCoverageEntity netCoverage) {
		this.netCoverage = netCoverage;
	}
	
}
