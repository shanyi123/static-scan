package com.ctrip.coverage.model.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "peter_nc")
public class NetCoverageEntity {
	Integer id;
    List<NetCoverageModuleEntity> modulelist=new ArrayList<NetCoverageModuleEntity>();
    List<SourceFileNamesEntity> sourceFileNamesList=new ArrayList<SourceFileNamesEntity>();
    String description;
	@Temporal(TemporalType.TIMESTAMP) 
	Date uploadTime;
	int linesCovered;
	int linesPartiallyCovered;
	int linesNotCovered;
	int blocksCovered;
	int blocksNotCovered;
	int totalLines;
	String version;
	float linesRate;
	float linesNoRate;
	float linesTotalCount;
	float blocksRate;
	String meragePath;
	//Solution
	TfsFolderEntity tfsFolder;
	


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@OneToMany(cascade={CascadeType.ALL},mappedBy="netCoverage")
	public List<NetCoverageModuleEntity> getModulelist() {
		return modulelist;
	}
	public void setModulelist(List<NetCoverageModuleEntity> modulelist) {
		this.modulelist = modulelist;
	}
	
	@ManyToOne
	@JoinColumn(name="tfsfolder_id")
	public TfsFolderEntity getTfsFolder() {
		return tfsFolder;
	}
	public void setTfsFolder(TfsFolderEntity tfsFolder) {
		this.tfsFolder = tfsFolder;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getUploadTime() {
		return uploadTime;
	}
	public void setUploadTime(Date uploadTime) {
		this.uploadTime = uploadTime;
	}
	public int getLinesCovered() {
		return linesCovered;
	}
	public void setLinesCovered(int linesCovered) {
		this.linesCovered = linesCovered;
	}
	public int getLinesPartiallyCovered() {
		return linesPartiallyCovered;
	}
	public void setLinesPartiallyCovered(int linesPartiallyCovered) {
		this.linesPartiallyCovered = linesPartiallyCovered;
	}
	public int getLinesNotCovered() {
		return linesNotCovered;
	}
	public void setLinesNotCovered(int linesNotCovered) {
		this.linesNotCovered = linesNotCovered;
	}
	public int getBlocksCovered() {
		return blocksCovered;
	}
	public void setBlocksCovered(int blocksCovered) {
		this.blocksCovered = blocksCovered;
	}
	public int getBlocksNotCovered() {
		return blocksNotCovered;
	}
	public void setBlocksNotCovered(int blocksNotCovered) {
		this.blocksNotCovered = blocksNotCovered;
	}
	public String getVersion()
	{
		return version;
	}
	public void setVersion(String version)
	{
		this.version = version;
	}
	@Transient
	public float getLinesRate() {
		return linesRate;
	}
	public void setLinesRate(float linesRate) {
		this.linesRate = linesRate;
	}
	@Transient
	public float getLinesTotalCount() {
		return linesTotalCount;
	}
	public void setLinesTotalCount(float linesTotalCount) {
		this.linesTotalCount = linesTotalCount;
	}
	@Transient
	public float getBlocksRate() {
		return blocksRate;
	}
	public void setBlocksRate(float blocksRate) {
		this.blocksRate = blocksRate;
	}
	@OneToMany(cascade={CascadeType.ALL},mappedBy="netCoverage")
	public List<SourceFileNamesEntity> getSourceFileNamesList() {
		return sourceFileNamesList;
	}
	public void setSourceFileNamesList(List<SourceFileNamesEntity> sourceFileNamesList) {
		this.sourceFileNamesList = sourceFileNamesList;
	}
	@Column(name="merage_path")
	public String getMeragePath() {
		return meragePath;
	}
	public void setMeragePath(String meragePath) {
		this.meragePath = meragePath;
	}
	@Transient
	public int getTotalLines() {
		return totalLines;
	}
	public void setTotalLines(int totalLines) {
		this.totalLines = totalLines;
	}
	@Transient
	public float getLinesNoRate() {
		return linesNoRate;
	}
	public void setLinesNoRate(float linesNoRate) {
		this.linesNoRate = linesNoRate;
	}
	

}
