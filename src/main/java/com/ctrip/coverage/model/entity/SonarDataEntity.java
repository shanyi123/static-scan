package com.ctrip.coverage.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "peter_sonar_data")
public class SonarDataEntity implements Serializable

{
	protected static final long serialVersionUID = 4L;

	private Integer id;
	private TfsFolderEntity tfsFolder;
	private Long projectId;
	private Long newestSnapId;
	private String projectName;
	private String version;
	// **违规代码行数据统计

	// 违规总行数
	private Long violations;
	// 阻断行数
	private Long blocker_violations;
	// 严重行数
	private Long critical_violations;
	// 重要行数
	private Long major_violations;
	// 次要行数
	private Long minor_violations;
	// 信息提示行数
	private Long info_violations;

	// **代码行、类、包数量统计

	// 代码行数
	private Long ncloc;
	
	private String sonar_log;
	private Integer result;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "tfsfolder_id")
	public TfsFolderEntity getTfsFolder() {
		return tfsFolder;
	}

	public void setTfsFolder(TfsFolderEntity tfsFolder) {
		this.tfsFolder = tfsFolder;
	}

	public Long getViolations() {
		return violations;
	}

	public void setViolations(Long violations) {
		this.violations = violations;
	}


	public Long getBlocker_violations() {
		return blocker_violations;
	}

	public void setBlocker_violations(Long blocker_violations) {
		this.blocker_violations = blocker_violations;
	}

	public Long getCritical_violations() {
		return critical_violations;
	}

	public void setCritical_violations(Long critical_violations) {
		this.critical_violations = critical_violations;
	}

	public Long getMajor_violations() {
		return major_violations;
	}

	public void setMajor_violations(Long major_violations) {
		this.major_violations = major_violations;
	}

	public Long getMinor_violations() {
		return minor_violations;
	}

	public void setMinor_violations(Long minor_violations) {
		this.minor_violations = minor_violations;
	}

	public Long getInfo_violations() {
		return info_violations;
	}

	public void setInfo_violations(Long info_violations) {
		this.info_violations = info_violations;
	}

	public Long getNcloc() {
		return ncloc;
	}

	public void setNcloc(Long ncloc) {
		this.ncloc = ncloc;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getNewestSnapId() {
		return newestSnapId;
	}

	public void setNewestSnapId(Long newestSnapId) {
		this.newestSnapId = newestSnapId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getSonar_log() {
		return sonar_log;
	}

	public void setSonar_log(String sonar_log) {
		this.sonar_log = sonar_log;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}



}
