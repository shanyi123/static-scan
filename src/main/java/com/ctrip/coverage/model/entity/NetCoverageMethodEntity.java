package com.ctrip.coverage.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "peter_nc_method")
public class NetCoverageMethodEntity
{

	Integer id;
	int blocksCovered;
	int blocksNotCovered;
	int linesCovered;
	int linesNotCovered;
	int linesPartiallyCovered;
	String methodKeyName;
	String methodName;
	String methodFullName;
	
	List<NetCoverageLinesEntity> linesList=new ArrayList<NetCoverageLinesEntity>();
	NetCoverageClassEntity netCoverageClass;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public int getBlocksCovered() {
		return blocksCovered;
	}
	public void setBlocksCovered(int blocksCovered) {
		this.blocksCovered = blocksCovered;
	}
	public int getBlocksNotCovered() {
		return blocksNotCovered;
	}
	public void setBlocksNotCovered(int blocksNotCovered) {
		this.blocksNotCovered = blocksNotCovered;
	}
	public int getLinesCovered() {
		return linesCovered;
	}
	public void setLinesCovered(int linesCovered) {
		this.linesCovered = linesCovered;
	}
	public int getLinesNotCovered() {
		return linesNotCovered;
	}
	public void setLinesNotCovered(int linesNotCovered) {
		this.linesNotCovered = linesNotCovered;
	}
	public int getLinesPartiallyCovered() {
		return linesPartiallyCovered;
	}
	public void setLinesPartiallyCovered(int linesPartiallyCovered) {
		this.linesPartiallyCovered = linesPartiallyCovered;
	}
	
	public String getMethodKeyName()
	{
		return methodKeyName;
	}
	public void setMethodKeyName(String methodKeyName)
	{
		this.methodKeyName = methodKeyName;
	}
	@Column(length=1024)
	public String getMethodName()
	{
		return methodName;
	}
	public void setMethodName(String methodName)
	{
		this.methodName = methodName;
	}
	@Column(length=1024)
	public String getMethodFullName()
	{
		return methodFullName;
	}
	public void setMethodFullName(String methodFullName)
	{
		this.methodFullName = methodFullName;
	}
	@OneToMany(cascade={CascadeType.ALL},mappedBy="netCoverageMethod")
	public List<NetCoverageLinesEntity> getLinesList()
	{
		return linesList;
	}
	public void setLinesList(List<NetCoverageLinesEntity> linesList)
	{
		this.linesList = linesList;
	}
	@ManyToOne
	@JoinColumn(name="nc_class_id")
	public NetCoverageClassEntity getNetCoverageClass()
	{
		return netCoverageClass;
	}
	public void setNetCoverageClass(NetCoverageClassEntity netCoverageClass)
	{
		this.netCoverageClass = netCoverageClass;
	}


	

}
