package com.ctrip.coverage.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "peter_nc_sourcefilenames")
public class SourceFileNamesEntity {
	Integer id;

	int sourceFileID;
	String sourceFileName;
	NetCoverageEntity netCoverage;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getSourceFileID() {
		return sourceFileID;
	}
	public void setSourceFileID(int sourceFileID) {
		this.sourceFileID = sourceFileID;
	}
	public String getSourceFileName() {
		return sourceFileName;
	}
	public void setSourceFileName(String sourceFileName) {
		this.sourceFileName = sourceFileName;
	}
	@ManyToOne
	@JoinColumn(name="nc_id")
	public NetCoverageEntity getNetCoverage() {
		return netCoverage;
	}
	public void setNetCoverage(NetCoverageEntity netCoverage) {
		this.netCoverage = netCoverage;
	}
	


	
}
