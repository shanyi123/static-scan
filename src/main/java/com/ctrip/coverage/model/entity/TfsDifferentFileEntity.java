package com.ctrip.coverage.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="peter_tfs_differentfile")
public class TfsDifferentFileEntity {
	int id;
	String path;
	int changeType;
	TfsScanEntity tfsScan;
	String modifyContent;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getChangeType() {
		return changeType;
	}
	public void setChangeType(int changeType) {
		this.changeType = changeType;
	}
	@ManyToOne
	@JoinColumn(name="tfsscan_id")
	public TfsScanEntity getTfsScan() {
		return tfsScan;
	}
	public void setTfsScan(TfsScanEntity tfsScan) {
		this.tfsScan = tfsScan;
	}
	public String getModifyContent() {
		return modifyContent;
	}
	public void setModifyContent(String modifyContent) {
		this.modifyContent = modifyContent;
	}
	
}
