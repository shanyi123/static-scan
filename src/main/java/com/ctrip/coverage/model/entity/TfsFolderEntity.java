package com.ctrip.coverage.model.entity;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name="peter_tfs_folder")
public class TfsFolderEntity {
	Integer id;
	String name;
	String serverPath;
	String projectPath;
	Integer type;
	Integer isCoverage;
	Integer isChange;
	Integer isCodeScan;
	TfsFolderEntity parent;
	private Integer parentId;
	private String nodeKey;
	private Collection<TfsFolderEntity> referenceTfsFolder;
    private Integer collection;
    private String projectName;
    private String applicationPath;
    private Integer codeLibraryType;
    private Integer language;
    private Integer scanRule;
    private String skippedProjectPattern;
	Set<TfsScanEntity> tfsScan=new HashSet<TfsScanEntity>();
	Set<NetCoverageEntity> netCoverageEntity=new HashSet<NetCoverageEntity>();
	Set<SonarDataEntity> sonarDataEntity=new HashSet<SonarDataEntity>();
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getServerPath() {
		return serverPath;
	}
	public void setServerPath(String serverPath) {
		this.serverPath = serverPath;
	}
	
	public String getProjectPath() {
		return projectPath;
	}
	public void setProjectPath(String projectPath) {
		this.projectPath = projectPath;
	}
	@OneToMany(cascade={CascadeType.REMOVE},mappedBy="tfsFolder")
	public Set<TfsScanEntity> getTfsScan() {
		return tfsScan;
	}
	public void setTfsScan(Set<TfsScanEntity> tfsScan) {
		this.tfsScan = tfsScan;
	}
	@OneToMany(cascade={CascadeType.ALL},mappedBy="tfsFolder")
	public Set<NetCoverageEntity> getNetCoverageEntity() {
		return netCoverageEntity;
	}
	public void setNetCoverageEntity(Set<NetCoverageEntity> netCoverageEntity) {
		this.netCoverageEntity = netCoverageEntity;
	}
	
	@OneToMany(cascade={CascadeType.ALL},mappedBy="tfsFolder")
	public Set<SonarDataEntity> getSonarDataEntity() {
		return sonarDataEntity;
	}
	public void setSonarDataEntity(Set<SonarDataEntity> sonarDataEntity) {
		this.sonarDataEntity = sonarDataEntity;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parentId", insertable = false, updatable = false)
	public TfsFolderEntity getParent() {
		return parent;
	}
	public void setParent(TfsFolderEntity parent) {
		this.parent = parent;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "id", insertable = false, updatable = false)
	public Collection<TfsFolderEntity> getReferenceTfsFolder() {
		return referenceTfsFolder;
	}
	public void setReferenceTfsFolder(Collection<TfsFolderEntity> referenceTfsFolder) {
		this.referenceTfsFolder = referenceTfsFolder;
	}
	public String getNodeKey() {
		return nodeKey;
	}
	public void setNodeKey(String nodeKey) {
		this.nodeKey = nodeKey;
	}
	public Integer getIsCoverage() {
		return isCoverage;
	}
	public void setIsCoverage(Integer isCoverage) {
		this.isCoverage = isCoverage;
	}
	public Integer getIsChange() {
		return isChange;
	}
	public void setIsChange(Integer isChange) {
		this.isChange = isChange;
	}
	public Integer getIsCodeScan() {
		return isCodeScan;
	}
	public void setIsCodeScan(Integer isCodeScan) {
		this.isCodeScan = isCodeScan;
	}
	public Integer getCollection() {
		return collection;
	}
	public void setCollection(Integer collection) {
		this.collection = collection;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getApplicationPath() {
		return applicationPath;
	}
	public void setApplicationPath(String applicationPath) {
		this.applicationPath = applicationPath;
	}
	public Integer getCodeLibraryType() {
		return codeLibraryType;
	}
	public void setCodeLibraryType(Integer codeLibraryType) {
		this.codeLibraryType = codeLibraryType;
	}
	public Integer getLanguage() {
		return language;
	}
	public void setLanguage(Integer language) {
		this.language = language;
	}
	public Integer getScanRule() {
		return scanRule;
	}
	public void setScanRule(Integer scanRule) {
		this.scanRule = scanRule;
	}
	public String getSkippedProjectPattern() {
		return skippedProjectPattern;
	}
	public void setSkippedProjectPattern(String skippedProjectPattern) {
		this.skippedProjectPattern = skippedProjectPattern;
	}
	
}
