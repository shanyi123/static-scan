package com.ctrip.coverage.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name="peter_tfs_sourcefileNamespace")
public class TfsSourceFileNamespaceEntity
{
	
	int id;
	String sourcefileAddr;
	String namespace;
	int namespaceStartLine;
	int namespaceEndLine;
	List<TfsClassEntity> classes = new ArrayList<TfsClassEntity>(); 
	NetCoverageMergeEntity netCoverageMerge = new NetCoverageMergeEntity();
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getSourcefileAddr()
	{
		return sourcefileAddr;
	}
	public void setSourcefileAddr(String sourcefileAddr)
	{
		this.sourcefileAddr = sourcefileAddr;
	}
	public String getNamespace()
	{
		return namespace;
	}
	public void setNamespace(String namespace)
	{
		this.namespace = namespace;
	}
	public int getNamespaceStartLine()
	{
		return namespaceStartLine;
	}
	public void setNamespaceStartLine(int namespaceStartLine)
	{
		this.namespaceStartLine = namespaceStartLine;
	}
	public int getNamespaceEndLine()
	{
		return namespaceEndLine;
	}
	public void setNamespaceEndLine(int namespaceEndLine)
	{
		this.namespaceEndLine = namespaceEndLine;
	}
	
	@OneToMany(cascade={CascadeType.REMOVE},mappedBy="tfsSourceFileNamespace")
	public List<TfsClassEntity> getClasses()
	{
		return classes;
	}
	public void setClasses(List<TfsClassEntity> classes)
	{
		this.classes = classes;
	}
	@ManyToOne
	@JoinColumn(name="netcoveragemerge_id")
	public NetCoverageMergeEntity getNetCoverageMerge()
	{
		return netCoverageMerge;
	}
	public void setNetCoverageMerge(NetCoverageMergeEntity netCoverageMerge)
	{
		this.netCoverageMerge = netCoverageMerge;
	}
}
