package com.ctrip.coverage.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="peter_tfs_method")
public class TfsMethodEntity
{
	int id;
	String methodName;
	int methodStartLine;
	int methodEndLine;
	int changeStatus;
	TfsClassEntity tfsClass;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getMethodName()
	{
		return methodName;
	}
	public void setMethodName(String methodName)
	{
		this.methodName = methodName;
	}
	public int getMethodStartLine()
	{
		return methodStartLine;
	}
	public void setMethodStartLine(int methodStartLine)
	{
		this.methodStartLine = methodStartLine;
	}
	public int getMethodEndLine()
	{
		return methodEndLine;
	}
	public void setMethodEndLine(int methodEndLine)
	{
		this.methodEndLine = methodEndLine;
	}
	public int getChangeStatus()
	{
		return changeStatus;
	}
	public void setChangeStatus(int changeStatus)
	{
		this.changeStatus = changeStatus;
	}
	@ManyToOne
	@JoinColumn(name="tfs_class_id")
	public TfsClassEntity getTfsClass()
	{
		return tfsClass;
	}
	public void setTfsClass(TfsClassEntity tfsClass)
	{
		this.tfsClass = tfsClass;
	}

}
