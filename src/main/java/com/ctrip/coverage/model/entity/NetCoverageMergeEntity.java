package com.ctrip.coverage.model.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "peter_nc_merge")
public class NetCoverageMergeEntity {
	int id;
	@Temporal(TemporalType.TIMESTAMP) 
//	Date MergeTime;
	//描述
//	String description;
	//扫描的开始版本号
//	String startVersion;
	//扫描的终止版本号
//	String endVersion;
	int startVersionId;
	int endVersionId;
	String serverPath;
	
	

	Set<TfsSourceFileNamespaceEntity> tfsSourceFileNamespace = new HashSet<TfsSourceFileNamespaceEntity>();
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

//	public Date getMergeTime()
//	{
//		return MergeTime;
//	}
//
//	public void setMergeTime(Date mergeTime)
//	{
//		MergeTime = mergeTime;
//	}
//
//	public String getDescription()
//	{
//		return description;
//	}
//
//	public void setDescription(String description)
//	{
//		this.description = description;
//	}

//	public String getStartVersion()
//	{
//		return startVersion;
//	}
//
//	public void setStartVersion(String startVersion)
//	{
//		this.startVersion = startVersion;
//	}
//
//	public String getEndVersion()
//	{
//		return endVersion;
//	}
//
//	public void setEndVersion(String endVersion)
//	{
//		this.endVersion = endVersion;
//	}

	public int getStartVersionId()
	{
		return startVersionId;
	}

	public void setStartVersionId(int startVersionId)
	{
		this.startVersionId = startVersionId;
	}

	public int getEndVersionId()
	{
		return endVersionId;
	}

	public void setEndVersionId(int endVersionId)
	{
		this.endVersionId = endVersionId;
	}

	public String getServerPath()
	{
		return serverPath;
	}

	public void setServerPath(String serverPath)
	{
		this.serverPath = serverPath;
	}
	@OneToMany(cascade={CascadeType.REMOVE},mappedBy="netCoverageMerge")
	public Set<TfsSourceFileNamespaceEntity> getTfsSourceFileNamespace()
	{
		return tfsSourceFileNamespace;
	}

	public void setTfsSourceFileNamespace(
			Set<TfsSourceFileNamespaceEntity> tfsSourceFileNamespace)
	{
		this.tfsSourceFileNamespace = tfsSourceFileNamespace;
	}

	
	
	
	
}
