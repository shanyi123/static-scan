package com.ctrip.coverage.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name="peter_tfs_class")
public class TfsClassEntity
{
	
	int id;
	String className;
	int classStartLine;
	int classEndLine;
	List<TfsMethodEntity> methods = new ArrayList<TfsMethodEntity>(); 
	TfsSourceFileNamespaceEntity tfsSourceFileNamespace;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	
	public String getClassName()
	{
		return className;
	}
	public void setClassName(String className)
	{
		this.className = className;
	}
	public int getClassStartLine()
	{
		return classStartLine;
	}
	public void setClassStartLine(int classStartLine)
	{
		this.classStartLine = classStartLine;
	}
	public int getClassEndLine()
	{
		return classEndLine;
	}
	public void setClassEndLine(int classEndLine)
	{
		this.classEndLine = classEndLine;
	}
	@OneToMany(cascade={CascadeType.REMOVE},mappedBy="tfsClass")
	public List<TfsMethodEntity> getMethods()
	{
		return methods;
	}
	public void setMethods(List<TfsMethodEntity> methods)
	{
		this.methods = methods;
	}
	@ManyToOne
	@JoinColumn(name="tfs_sourcefilenamespace_id")
	public TfsSourceFileNamespaceEntity getTfsSourceFileNamespace()
	{
		return tfsSourceFileNamespace;
	}
	public void setTfsSourceFileNamespace(
			TfsSourceFileNamespaceEntity tfsSourceFileNamespace)
	{
		this.tfsSourceFileNamespace = tfsSourceFileNamespace;
	}
}
