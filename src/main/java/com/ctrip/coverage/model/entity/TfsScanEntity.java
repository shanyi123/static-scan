package com.ctrip.coverage.model.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="peter_tfs_scan")

public class TfsScanEntity {
	int id;
	@Temporal(TemporalType.TIMESTAMP) 
	//扫描开始时间
	Date scanTime;
	//描述
	String description;
	//扫描的开始版本号
	String startVersion;
	//扫描的终止版本号
	String endVersion;
	//终止版本的总文件数
	int totalFileCount;
	//新增文件数
	int addFileCount;
	//删除文件数
	int deleteFileCount;
	//删除文件数
	int modifyFileCount;
	//所有变更文件综述
	int allChangedFileCount;
	//变更率（allchangedFileCount/totalFileCount）
	float changeRate;
	//所属目录
	TfsFolderEntity tfsFolder;
	//下属的differentfile
	Set<TfsDifferentFileEntity> tfsDifferentFile=new HashSet<TfsDifferentFileEntity>();
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getScanTime() {
		return scanTime;
	}
	public void setScanTime(Date scanTime) {
		this.scanTime = scanTime;
	}
	public String getStartVersion() {
		return startVersion;
	}
	public void setStartVersion(String startVersion) {
		this.startVersion = startVersion;
	}
	public String getEndVersion() {
		return endVersion;
	}
	public void setEndVersion(String endVersion) {
		this.endVersion = endVersion;
	}
	public int getTotalFileCount() {
		return totalFileCount;
	}
	public void setTotalFileCount(int totalFileCount) {
		this.totalFileCount = totalFileCount;
	}
	public int getAddFileCount() {
		return addFileCount;
	}
	public void setAddFileCount(int addFileCount) {
		this.addFileCount = addFileCount;
	}
	public int getDeleteFileCount() {
		return deleteFileCount;
	}
	public void setDeleteFileCount(int deleteFileCount) {
		this.deleteFileCount = deleteFileCount;
	}
	public int getModifyFileCount() {
		return modifyFileCount;
	}
	public void setModifyFileCount(int modifyFileCount) {
		this.modifyFileCount = modifyFileCount;
	}
	public int getAllChangedFileCount() {
		return allChangedFileCount;
	}
	public void setAllChangedFileCount(int allChangedFileCount) {
		this.allChangedFileCount = allChangedFileCount;
	}
	public float getChangeRate() {
		return changeRate;
	}
	public void setChangeRate(float changeRate) {
		this.changeRate = changeRate;
	}
	@ManyToOne
	@JoinColumn(name="tfsfolder_id")
	public TfsFolderEntity getTfsFolder() {
		return tfsFolder;
	}
	public void setTfsFolder(TfsFolderEntity tfsFolder) {
		this.tfsFolder = tfsFolder;
	}
	
	@OneToMany(cascade={CascadeType.REMOVE},mappedBy="tfsScan")
	public Set<TfsDifferentFileEntity> getTfsDifferentFile() {
		return tfsDifferentFile;
	}
	public void setTfsDifferentFile(Set<TfsDifferentFileEntity> tfsDifferentFile) {
		this.tfsDifferentFile = tfsDifferentFile;
	}
}
