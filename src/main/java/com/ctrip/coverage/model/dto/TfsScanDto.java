package com.ctrip.coverage.model.dto;

public class TfsScanDto {
	
	String startVersion;
	
	String endVersion;
	
	String description;
	
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getStartVersion() {
		return startVersion;
	}
	public void setStartVersion(String startVersion) {
		this.startVersion = startVersion;
	}
	public String getEndVersion() {
		return endVersion;
	}
	public void setEndVersion(String endVersion) {
		this.endVersion = endVersion;
	}
	
	
	
}
