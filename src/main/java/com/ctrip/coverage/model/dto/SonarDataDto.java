package com.ctrip.coverage.model.dto;

import java.io.Serializable;
public class SonarDataDto implements Serializable

{
	private static final long serialVersionUID = -7970848646314840509L;

    private Long projectId;
	private Long newestSnapId;
	private String projectName;
	private String version;
	// **违规代码行数据统计

	// 违规总行数
	private Long violations;
	// 合规比例
	private float complianceRulesRate;
	// 阻断行数
	private Long blocker_violations;
	// 严重行数
	private Long critical_violations;
	// 重要行数
	private Long major_violations;
	// 次要行数
	private Long minor_violations;
	// 信息提示行数
	private Long info_violations;

	// **代码行、类、包数量统计

	// 代码行数
	private Long ncloc;
	// 代码文件数
	private Long codeFileCount;
	// 类数
	private Long classes;
	// 方法数
	private Long methodCount;

	// **注释重复度统计

	// 注释比例
	private Long commentsRate;
	// 注释行数
	private Long commentsCount;
	// 代码重复比例
	private float DuplicationsRate;
	// 代码重复行数
	private Long DuplicationsCount;

	// **复杂度统计

	// 方法复杂度
	private Long methodComplexity;
	// 类复杂度
	private Long classComplexity;
	// 文件复杂度
	private Long fileComplexity;
	
	public Long getViolations() {
		return violations;
	}
	public void setViolations(Long violations) {
		this.violations = violations;
	}
	public float getComplianceRulesRate() {
		return complianceRulesRate;
	}
	public void setComplianceRulesRate(float complianceRulesRate) {
		this.complianceRulesRate = complianceRulesRate;
	}
	
	public Long getCodeFileCount() {
		return codeFileCount;
	}
	public void setCodeFileCount(Long codeFileCount) {
		this.codeFileCount = codeFileCount;
	}
	public Long getClasses() {
		return classes;
	}
	public void setClasses(Long classes) {
		this.classes = classes;
	}
	public Long getMethodCount() {
		return methodCount;
	}
	public void setMethodCount(Long methodCount) {
		this.methodCount = methodCount;
	}
	public Long getCommentsRate() {
		return commentsRate;
	}
	public void setCommentsRate(Long commentsRate) {
		this.commentsRate = commentsRate;
	}
	public Long getCommentsCount() {
		return commentsCount;
	}
	public void setCommentsCount(Long commentsCount) {
		this.commentsCount = commentsCount;
	}
	public float getDuplicationsRate() {
		return DuplicationsRate;
	}
	public void setDuplicationsRate(float duplicationsRate) {
		DuplicationsRate = duplicationsRate;
	}
	public Long getDuplicationsCount() {
		return DuplicationsCount;
	}
	public void setDuplicationsCount(Long duplicationsCount) {
		DuplicationsCount = duplicationsCount;
	}
	public Long getMethodComplexity() {
		return methodComplexity;
	}
	public void setMethodComplexity(Long methodComplexity) {
		this.methodComplexity = methodComplexity;
	}
	public Long getClassComplexity() {
		return classComplexity;
	}
	public void setClassComplexity(Long classComplexity) {
		this.classComplexity = classComplexity;
	}
	public Long getFileComplexity() {
		return fileComplexity;
	}
	public void setFileComplexity(Long fileComplexity) {
		this.fileComplexity = fileComplexity;
	}
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
	public Long getNewestSnapId() {
		return newestSnapId;
	}
	public void setNewestSnapId(Long newestSnapId) {
		this.newestSnapId = newestSnapId;
	}
	public Long getBlocker_violations() {
		return blocker_violations;
	}
	public void setBlocker_violations(Long blocker_violations) {
		this.blocker_violations = blocker_violations;
	}
	public Long getCritical_violations() {
		return critical_violations;
	}
	public void setCritical_violations(Long critical_violations) {
		this.critical_violations = critical_violations;
	}
	public Long getMajor_violations() {
		return major_violations;
	}
	public void setMajor_violations(Long major_violations) {
		this.major_violations = major_violations;
	}
	public Long getMinor_violations() {
		return minor_violations;
	}
	public void setMinor_violations(Long minor_violations) {
		this.minor_violations = minor_violations;
	}
	public Long getInfo_violations() {
		return info_violations;
	}
	public void setInfo_violations(Long info_violations) {
		this.info_violations = info_violations;
	}
	public Long getNcloc() {
		return ncloc;
	}
	public void setNcloc(Long ncloc) {
		this.ncloc = ncloc;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public Long getProjectId() {
		return projectId;
	}
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	@Override
	public String toString() {
		return "SonarDataDto [newestSnapId=" + newestSnapId + ", version=" + version + ", violations=" + violations + ", complianceRulesRate=" + complianceRulesRate
				+ ", blocker_violations=" + blocker_violations + ", critical_violations=" + critical_violations + ", major_violations=" + major_violations + ", minor_violations="
				+ minor_violations + ", info_violations=" + info_violations + ", ncloc=" + ncloc + ", codeFileCount=" + codeFileCount + ", classes=" + classes + ", methodCount="
				+ methodCount + ", commentsRate=" + commentsRate + ", commentsCount=" + commentsCount + ", DuplicationsRate=" + DuplicationsRate + ", DuplicationsCount="
				+ DuplicationsCount + ", methodComplexity=" + methodComplexity + ", classComplexity=" + classComplexity + ", fileComplexity=" + fileComplexity + "]";
	}
	
   
}
