package com.ctrip.coverage.model.dto;

public class BuildDto {
	private String localPath;
	private Boolean isBuildSuc;
	private Integer collection;
	private String serverPath;
	private String version;
	private String projectName;

	public String getLocalPath() {
		return localPath;
	}

	public void setLocalPath(String localPath) {
		this.localPath = localPath;
	}

	public Boolean getIsBuildSuc() {
		return isBuildSuc;
	}

	public void setIsBuildSuc(Boolean isBuildSuc) {
		this.isBuildSuc = isBuildSuc;
	}

	public Integer getCollection() {
		return collection;
	}

	public void setCollection(Integer collection) {
		this.collection = collection;
	}

	public String getServerPath() {
		return serverPath;
	}

	public void setServerPath(String serverPath) {
		this.serverPath = serverPath;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

}
