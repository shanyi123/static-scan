package com.ctrip.coverage.model.dto;

public class CoverageDto {
	int id;
	int tfsFolderId;
	String description;
	String uploadTime;
	int linesCovered;
	int linesPartiallyCovered;
	int linesNotCovered;
	int blocksCovered;
	int blocksNotCovered;
	float lineRate;
	float blockRate;
	String version;

	String file;
	
	
	public int getTfsFolderId() {
		return tfsFolderId;
	}
	public void setTfsFolderId(int tfsFolderId) {
		this.tfsFolderId = tfsFolderId;
	}
	public String getVersion()
	{
		return version;
	}
	public void setVersion(String version)
	{
		this.version = version;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public float getLineRate() {
		return lineRate;
	}
	public void setLineRate(float lineRate) {
		this.lineRate = lineRate;
	}
	public float getBlockRate() {
		return blockRate;
	}
	public void setBlockRate(float blockRate) {
		this.blockRate = blockRate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getUploadTime() {
		return uploadTime;
	}
	public void setUploadTime(String uploadTime) {
		this.uploadTime = uploadTime;
	}
	public int getLinesCovered() {
		return linesCovered;
	}
	public void setLinesCovered(int linesCovered) {
		this.linesCovered = linesCovered;
	}
	public int getLinesPartiallyCovered() {
		return linesPartiallyCovered;
	}
	public void setLinesPartiallyCovered(int linesPartiallyCovered) {
		this.linesPartiallyCovered = linesPartiallyCovered;
	}
	public int getLinesNotCovered() {
		return linesNotCovered;
	}
	public void setLinesNotCovered(int linesNotCovered) {
		this.linesNotCovered = linesNotCovered;
	}
	public int getBlocksCovered() {
		return blocksCovered;
	}
	public void setBlocksCovered(int blocksCovered) {
		this.blocksCovered = blocksCovered;
	}
	public int getBlocksNotCovered() {
		return blocksNotCovered;
	}
	public void setBlocksNotCovered(int blocksNotCovered) {
		this.blocksNotCovered = blocksNotCovered;
	}
}
