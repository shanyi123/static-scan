package com.ctrip.coverage.model.dto;

public class BuildPublishDto {
	private Integer tfsCollection;
	private String serverPath;
	private String localPath;
	private String projectName;


	public Integer getTfsCollection() {
		return tfsCollection;
	}

	public void setTfsCollection(Integer tfsCollection) {
		this.tfsCollection = tfsCollection;
	}

	public String getServerPath() {
		return serverPath;
	}

	public void setServerPath(String serverPath) {
		this.serverPath = serverPath;
	}

	public String getLocalPath() {
		return localPath;
	}

	public void setLocalPath(String localPath) {
		this.localPath = localPath;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}


}
