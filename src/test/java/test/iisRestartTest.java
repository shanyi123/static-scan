package test;

import java.io.BufferedReader;
import java.io.File;

import org.junit.Test;

import com.ctrip.coverage.utils.StreamGobbler;

public class iisRestartTest {

	@Test
	public void test() {
		Process proc;
		Runtime rt = Runtime.getRuntime();
		String command;
		String str;
		BufferedReader bufferedReader;
		command = "cmd.exe /c  start /b D:\\BAT\\SitRestart.bat ";
		try {
			proc = rt.exec(command, null, new File("C:\\Windows\\System32\\inetsrv"));
			StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "Error");
			StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "Output");
			errorGobbler.start();
			outputGobbler.start();
			proc.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
